import React, { Component } from 'react';
var jwt = require('jsonwebtoken');

export function getToken(token) {

    let res = {
        status: false,
        value: ''
    };

    // verify a token symmetric
    jwt.verify(token, 'shhhhh', function (err, decoded) {
        // console.log(decoded.foo) // bar
        if(err){
            res = {
                status : false
            };
        }else{
            res = {
                status: true,
                value: decoded.foo
            };
        }
    });

    return res;
}