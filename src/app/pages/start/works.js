import React, { Component } from 'react';



class Works extends Component {
    render() {
        return (
            <>
                <section class="how-work">
                    <div class="container">
                        <div class="heading text-center">
                            <h2>How it works</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="figure">
                                    <img src="images/img5.png" />
                                </div>
                                <div class="figcaption">
                                    <h5>Classes for all Ages</h5>
                                    <p>Live teachers lead classes and ensure that they are engaging and fun for the kids.</p>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="figure">
                                    <img src="images/img5.png" />
                                </div>
                                <div class="figcaption">
                                    <h5>Personalized Plan</h5>
                                    <p>Teachers give personal attention to kids and ensure that no kid is left behind. The classes are designed to be interactive and fun.</p>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="figure">
                                    <img src="images/img5.png" />
                                </div>
                                <div class="figcaption">
                                    <h5>Small Class Size</h5>
                                    <p>Your kids can learn with a group or schedule 1:1 sessions with our eminent teachers. What works for you, works for us!</p>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <div class="figure">
                                    <img src="images/img5.png" />
                                </div>
                                <div class="figcaption">
                                    <h5>Holistic Development</h5>
                                    <p>Live teachers lead classes and ensure that they are engaging and fun for the kids.</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="figure">
                                    <img src="images/img5.png" />
                                </div>
                                <div class="figcaption">
                                    <h5>Explore them all</h5>
                                    <p>Teachers give personal attention to kids and ensure that no kid is left behind. The classes are designed to be interactive and fun.</p>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}

export default Works;