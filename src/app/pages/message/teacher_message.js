import React, { useState, useEffect, useRef } from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar';
import { useHistory } from "react-router-dom";

import _ from 'lodash';
import moment from 'moment';
import axios from "axios";
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setData, getData } from '../../util/localStorageHandler.js';

import Header from '../layout/header.js';
import SubHeader from '../layout/subheader.js';
import Footer from '../layout/footer.js';


function TeacherMessage() {

    const history = useHistory();
    const fileRef = useRef(null);

    const [loading, setLoading] = useState(false);
    const [userId, setUserId] = useState(false);
    const [userType, setUserType] = useState(false);
    const [messages, setMessages] = useState([]);
    const [classes, setClasses] = useState([]);
    const [teachers, setTeachers] = useState([]);
    const [className, setClassName] = useState("");
    const [classId, setClassId] = useState("");
    const [textMessage, setTextMessage] = useState("");
    const [fileUrl, setFileUrl] = useState("");
    const [fileUploaded, setFileUploaded] = useState("");


    useEffect(() => {
        let data = getData();
        if (data) {
            setUserId(data.userId);
            setUserType(data.userType);
            getClasses(data.userId);
            // getMessages(data.userId);
            getAllTeachers();
        }
    }, []);

    const getClasses = async (userId) => {
        setLoading(true);
        let dataToSend = {
            "userId": userId
        };
        let path = `${API_BASEURL}/class/fetch-class-by-userid-of-teacher`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setLoading(false);
            res.result && setClasses(res.result);
            // console.log("response from all classes ==", res.result);
        } else {
            setClasses([]);
            setLoading(false);
        }

    }

    const classSelection = (classId) => {

        // let selectedClass = classes[id];
        // selectedClass && selectedClass.className && setClassName(selectedClass.className);
        // if (selectedClass && selectedClass.classId) {
        getMessages(classId);
        setClassId(classId);
        // }

    }

    const getMessages = async (classId) => {
        if (classId) {
            setMessages([]);
            setLoading(true);
            let dataToSend = {
                "userId": userId,
                "classId": classId,
                "page": "",
                "limit": ""
            };
            let path = `${API_BASEURL}/message/get-message-by-class-id`;
            let res = await post(path, dataToSend);
            // console.log("response from fetch message ==", res);
            if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
                setLoading(false);
                res.result && setMessages(res.result);
            } else {
                setLoading(false);
                setMessages([]);
            }
        }
    }

    const getAllTeachers = async () => {
        setLoading(true);
        let dataToSend = {
            "page": '',
            "limit": ''
        }

        let path = `${API_BASEURL}/user/list-of-teacher`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            // console.log("Response from list teacher==", res.result);
            setLoading(false);
            res.result && setTeachers(res.result);
        } else {
            setLoading(false);
            setTeachers([]);
        }

    }

    const onFileChange = async (e) => {
        e.preventDefault();
        // console.log("input file data ===", e.target.files[0].name);
        // let file = fileRef;
        let file = e.target.files[0];

        if (file && !loading) {
            let blnValid = false;
            let sFileName = file.name;
            var _validFileExtensions = [".mp4",".png",".jpg"];
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                Swal.fire('Error!', "File type is not valid.", 'error');
            } else {
                setLoading(true);
                var reader = new FileReader();
                var url = reader.readAsDataURL(file);

                const photo = new FormData();
                photo.append('file', file);
                photo.append('userId', userId);
                photo.append('userType', userType);
                photo.append('classId', classId);

                let headers = {
                    'Content-Type': 'multipart/form-data'
                }
                // if (authToken) headers['Authorization'] = `Bearer ${authToken}`;
                // console.log("header to be sent ===", headers);

                axios
                    .post(API_BASEURL + "/message/upload-file-for-message",
                        photo,
                        {
                            headers,
                            onUploadProgress: (progressEvent) => {
                                let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
                                setFileUploaded(uploadPercentage);
                            }
                        })
                    .then(serverResponse => {
                        // console.log("uploading response is ===", serverResponse);
                        let res = serverResponse.data;
                        if (res && res.serverResponse && !res.serverResponse.isError) {
                            Swal.fire('Success!', "Successfully Uploaded.", 'success');
                            // getUserData(authToken);
                            getMessages(classId);
                            setLoading(false);
                            setFileUploaded('');
                        } else {
                            setLoading(false);
                            setFileUploaded('');
                            res && Swal.fire('Error!', "Sorry! something went wrong.", 'error');
                        }
                    })
                    .catch(error => {
                        console.log(error);
                        setLoading(false);
                    })

            }

        }

    }

    const postMessage = async () => {

        if (classId) {
            if (textMessage || fileUrl) {
                setLoading(true);
                let dataToSend = {
                    "userId": userId,
                    "userType": userType,
                    "classId": classId,
                    "message": textMessage
                };

                let path = `${API_BASEURL}/message/add-message`;
                let res = await post(path, dataToSend);

                if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
                    setLoading(false);
                    setTextMessage("");
                    getMessages(classId);
                } else {
                    setLoading(false)
                    getMessages(classId);
                }

            } else {
                Swal.fire("Error!", "Please write or upload something.", "error");
            }
        } else {
            Swal.fire("Error!", "Please select class first.", "error");
        }
    }

    const deleteMessage = async (id) => {
        // let selectedMessage = messages[id];
        // let messageId = "";
        let dataToSend = {
            "messageId": id
        };

        let path = `${API_BASEURL}/message/delete-message`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setLoading(false);
            getMessages(classId);
            Swal.fire("Success!", "Successfully deleted.", "success");
        } else {
            setLoading(false);
            getMessages(classId);
            Swal.fire("Error!", "Something went wrong.", "error");
        }


    }

    const nameOfTeacher = (userId) => {
        if (teachers) {
            let selectedTeacher = _.filter(teachers, { userId: userId });
            if (selectedTeacher && selectedTeacher[0].name) {
                return selectedTeacher[0].name;
            }
        }else{
            return null;
        }
    }

    return (
        <>
            <Header />
            <section class="msg-box">

                <SubHeader tag={'message'} />
                <div class="container">
                    <br /><br /><br />
                    <div class="login-form">
                        <div>
                            <label>Select class:</label>
                            <select
                                type="select"
                                class="form-control"
                                value={classId}
                                onChange={e => { setClassId(e.target.value); classSelection(e.target.value); }}
                            >
                                <option value="">Select class</option>
                                {classes && classes.map((data, index) => {
                                    return <option key={index} value={data.classId} selected={classId == data.classId ? true : false}>
                                        {data.name}
                                    </option>
                                })}
                            </select>
                        </div>
                    </div>
                    <div class="msg">
                        <div class="text-sec">
                            <ul>
                                {messages && messages.map((data, index) => {
                                    return data.messageType == "MESSAGE" ? <li key={index}>
                                        {data.userType == "admin" ?
                                            <div>Admin</div>
                                            : data.userType == "teacher" ?
                                                <div>{nameOfTeacher(data.userId) ? nameOfTeacher(data.userId): null}</div>
                                                : <div></div>}
                                        <span>{data.message && data.message}</span>
                                        &nbsp; &nbsp;
                                        {data.userType == "teacher" ?<span onClick={() => deleteMessage(data.messageId)}><i class="fa fa-trash" aria-hidden="true"></i></span>:null}
                                    </li>
                                        : data.messageType == "FILE" ? <li class="video-sec">
                                            {data.userType == "admin" ?
                                                <div>Admin</div>
                                                : data.userType == "teacher" ?
                                                    <div>{nameOfTeacher(data.userId) ? nameOfTeacher(data.userId): null}</div>
                                                    : <div></div>}
                                            {/* <iframe width="100%" height="350" src={data.message} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> */}
                                            <video preload="auto" style={{ width: "100%" }}
                                                controls controlsList="nofullscreen nodownload"
                                            >
                                                <source src={data.message && data.message} />
                                            </video>
                                            &nbsp; &nbsp;
                                            {data.userType == "teacher" ?<span onClick={() => deleteMessage(data.messageId)}><i class="fa fa-trash" aria-hidden="true"></i></span>:null}
                                        </li>
                                            : null
                                })}

                            </ul>
                        </div>

                        <div class="input-group msg-type">
                            <input type="text" name="EMAIL" value={textMessage}
                                onChange={(e) => setTextMessage(e.target.value)}
                                class="form-control memail" placeholder="Post a message"
                            />
                            {classId ?
                                <input type="file" ref={fileRef} class="upload"
                                    onChange={(e) => { onFileChange(e) }}
                                    id="myFile" name="filename" disabled={loading ? true : false}
                                />
                                : null}
                            {!loading ?
                                <button class="btn" type="submit" onClick={() => postMessage()}>Post</button>
                                : <button class="btn" type="submit" >Post</button>}
                            {/* {loading?<span><i class="fa fa-spinner" aria-hidden="true"></i></span>:null} */}
                        </div>

                        {fileUploaded ? <ProgressBar striped variant="success" now={fileUploaded} label={`${fileUploaded}%`} /> : null}

                    </div>
                </div>
            </section>
            <Footer />
        </>
    )

}

export default TeacherMessage;