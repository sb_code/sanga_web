import React, { useState, useEffect } from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Header from '../layout/header.js';
import Footer from '../layout/footer.js';
import withAuthorization from '../../Session/withAuthorization.js';
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setData, getData } from '../../util/localStorageHandler.js';
import Subscription from './subscription.js';
import OneTimePay from './one_time_pay.js';


function ChoosePlan() {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [tag, setTag] = useState('sub');
    const [userId, setUserId] = useState('');
    const [plans, setPlans] = useState([]);
    const [selectPlan, setSelectPlan] = useState('');
    const [userDetails, setUserDetails] = useState('');
    const [inPlan, setInPlan] = useState(false);
    const [priceId, setPriceId] = useState("");

    // componentDidMount
    useEffect(() => {
        let data = getData();
        if (data) {
            setUserId(data.userId);
            getPlans();
            getUserDetails(data.userId);
        } else {
            history.push({
                pathname: '/'
            })
        }
    }, [])

    // componentDidUpdate - 
    useEffect(() => {
        if (userDetails && userDetails.priceId) {
            setPriceId(userDetails.priceId);
        } else {
            setPriceId("");
            setPlans();
            getPlans();
        }
    }, [setPriceId, userDetails])

    const getPlans = async () => {

        setLoading(true);
        let path = `${API_BASEURL}/payment/get-subscription-plans`;
        let res = await post(path);
        // console.log("Response from get plans", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode) {
            res.result && setPlans(res.result);
            setLoading(false);
        } else {
            setLoading(false);
            setPlans([]);
        }

    }

    const getUserDetails = async (userId) => {

        setLoading(true);
        let dataToSend = {
            'userId': userId
        };
        let path = `${API_BASEURL}/user/get-student-by-user-id`;
        let res = await post(path, dataToSend);
        // console.log("Response from get user details==", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode) {
            res.result && setUserDetails(res.result);
            res.result && res.result.isInSubscription ? setInPlan(true) : setInPlan(false);
            res.priceId ? setPriceId(res.priceId) : setPriceId("");
            setLoading(false);
        } else {
            setLoading(false);
            // setPlans([]);
        }

    }

    const choosePlan = (index) => {

        let selectedPlan = plans[index];
        setSelectPlan(selectedPlan);

    }

    const choosePayment = (index) => {
        let selectedPlan = plans[index];
        setSelectPlan(selectedPlan);
    }

    const payment = (status) => {
        if (selectPlan && selectPlan != '') {

            history.push({
                pathname: '/payment',
                state: {
                    planId: selectPlan.subscriptionPlanId,
                    priceId: selectPlan.priceId,
                    planName: selectPlan.planName,
                    amount: selectPlan.cost,
                    status: status
                }
            })

        } else {
            if (status == 'Subscription') {
                Swal.fire("Error", "Please choose a plan subscribe", "Error");
            } else if (status == 'change') {
                Swal.fire("Error", "Please choose another plan to update", "Error");
            } else if (status == 'Payment') {
                Swal.fire("Error", "Please choose a plan to purchase", "Error");
            }
        }

    }

    const canclePlan = async () => {

        setLoading(true);
        let dataToSend = {
            "userId": userId
        }
        let path = `${API_BASEURL}/payment/cancel-subscription-period-end`;
        let res = await post(path, dataToSend);
        console.log("res of cancel ==", res);
        if (res && res.serverResponse) {
            if (!res.serverResponse.isError) {
                getUserDetails(userId);
                Swal.fire("Succes", "Successfully cancled", "Succes");
                setLoading(false);
                setInPlan(false);
            } else {
                Swal.fire("Error", res.serverResponse.message, "Error");
                setLoading(false);
            }
        } else {
            Swal.fire("Error", "Sorry! Try again", "Error");
            setLoading(false);
        }


    }

    // For changing subscription tab-
    const subscriptionTag = () => {
        setTag('sub');
        setSelectPlan('');
    }

    // For changing one time pay tag - 
    const OneTimeTag = () => {
        setTag('pay');
        setSelectPlan('');
    }


    return (
        <>
            <Header />
            <section class="my-classes for-find-class">

                <div class="active-heading">
                    <ul>
                        <li class={tag && tag == 'sub' ? "active" : ""} onClick={() => subscriptionTag()} style={{ cursor: "pointer" }}>Subscription</li>
                        <li class={tag && tag == 'pay' ? "active" : ""} onClick={() => OneTimeTag()} style={{ cursor: "pointer" }}>One Time Pay</li>
                    </ul>
                </div>

                {/* <section class="plans sec_pad"> */}
                <section class="plans sec_pad">
                    <div class="container-fluid">
                        <div class="heading">
                            <h2 class="text-center">
                                {tag && tag == 'sub' ? "Choose Plan For Subscription" : ""}
                                {tag && tag == 'pay' ? "Choose Plan For Payment" : ""}
                            </h2>
                        </div>
                        {/* For Subscription plans- */}
                        {tag && tag == 'sub' ?
                            <div class="row">

                                {plans && plans.length > 0 && plans.map((data, index) => {
                                    return <div class="col-lg-3" key={index}>
                                        <div class="choose-plan text-center">
                                            <label class="form-check-label">
                                                {data.priceId == priceId ?
                                                    <input type="radio" class="form-check-input"
                                                        name="optradio" onClick={() => choosePlan(index)}
                                                        defaultChecked={true}
                                                    />
                                                    : priceId != "" ?
                                                        <input type="radio" class="form-check-input"
                                                            name="optradio" onClick={() => choosePlan(index)}
                                                        />
                                                        :
                                                        <input type="radio" class="form-check-input"
                                                            name="optradio" onClick={() => choosePlan(index)}
                                                        />}

                                            </label>

                                            <strong class="d-block">{data.planName && data.planName}</strong>
                                            <h3>{data.cost && '$' + data.cost}</h3>
                                            <span class="d-block">Per Month</span>
                                            <p>
                                                Plan Type: {!data.isUnlimited ? "Not Unlimited" : "Unlimited"}<br />
                                                No. of session: {!data.isUnlimited ? data.noOfSession && data.noOfSession : 'Unlimited'}
                                            </p>
                                        </div>
                                    </div>
                                })}

                                {!loading ?
                                    <>
                                        {!inPlan ?
                                            <a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => payment('Subscription')}>Start Free Trial</a>
                                            : <>
                                                <a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => payment('change')}>Update Plan</a>
                                                <a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => canclePlan()}>Cancel Plan</a>
                                            </>
                                        }

                                    </>
                                    : null}

                            </div>
                            : null}

                        {/* For one time pay plans - */}
                        {tag && tag == 'pay' ?
                            <>
                                <div class="row">

                                    {plans && plans.length > 0 && plans.map((data, index) => {
                                        return !data.isUnlimited && <div class="col-lg-4" key={index}>
                                            <div class="choose-plan text-center">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input"
                                                        name="optradio" onClick={() => choosePayment(index)}
                                                    />
                                                </label>

                                                <strong class="d-block">{data.planName && data.planName}</strong>
                                                <h3>{data.cost && '$' + data.cost}</h3>
                                                {/* <span class="d-block">Per Month</span> */}
                                                <p>
                                                    No. of session: {data.noOfSession && data.noOfSession}
                                                </p>
                                            </div>
                                        </div>
                                    })}

                                </div>
                                <div class="row">

                                    {!loading ?
                                        <>
                                            <a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => payment('Payment')}>Direct Payment</a>
                                        </>
                                        : null}
                                </div>
                            </>
                            : null}

                    </div>
                </section>

            </section>
            <Footer />
        </>
    )

}

const condition = authUser => authUser && authUser.userType === SG_STUDENT;
export default withAuthorization(condition)(ChoosePlan);