import React, { useState, useEffect } from 'react';
import Header from '../layout/header.js';
import SubHeader from '../layout/subheader.js';
import Footer from '../layout/footer.js';
import withAuthorization from '../../Session/withAuthorization.js';
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setData, getData } from '../../util/localStorageHandler.js';


function UserDashboard() {

	const history = useHistory();
	const [loading, setLoading] = useState(false);
	const [userId, setUserId] = useState('');
	const [recomendClass, setRecomendClass] = useState('');
	const [myClass, setMyClass] = useState('');

	useEffect(() => {
		let data = getData();
		if (data) {
			// internalLogin(data.userType);
			setUserId(data.userId);
			let recomendList = getRecomendClass(data.userId);
			let myClassList = getMyClassList(data.userId);

		} else {
			history.push({
				pathname: '/'
			})
		}
	}, []);


	const internalLogin = (userType) => {
		// console.log('here 3', userType);

		if (userType == SG_STUDENT) {
			history.push("/userdashboard");
		} else if (userType == SG_TEACHER) {
			history.push("/teachersdashboard");
		} else {
			Swal.fire("Error", 'userType not found', "error");
		}
	}

	const getRecomendClass = async (userid) => {

		setLoading(true);
		let dataToSend = {
			userId: userid ? userid : userId,
			// page: '',
			// limit: ''
		};

		let path = `${API_BASEURL}/class/get-suggested-sessions-by-student-user-id`;
		let res = await post(path, dataToSend);

		if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
			setRecomendClass(res.result && res.result)
			setLoading(false);
		} else {
			setRecomendClass("");
			setLoading(false);
		}


	}

	const getMyClassList = async (userid) => {

		setLoading(true);
		let dataToSend = {
			userId: userid ? userid : userId,
			// status: "",
			// page: '',
			// limit: ''
		};

		let path = `${API_BASEURL}/class/get-session-by-student-userId`;
		let res = await post(path, dataToSend);

		if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
			setMyClass(res.result && res.result)
			setLoading(false);
		} else {
			setMyClass("");
			setLoading(false);
		}

	}

	const isEnrolled = (enrolledStudent) => {

		if (enrolledStudent && enrolledStudent.length > 0) {
			for (let i = 0; i < enrolledStudent.length; i++) {
				if (enrolledStudent[i].userId == userId) {
					return true
				}
			}
		} else {
			return false;
		}

	}

	// For showing teacher's profile -
	const goToTeacherProfile = (teacherId) => {
		if (teacherId) {
			history.push({
				pathname: "/teacherprofile/" + `${teacherId}`,
				state: {
					'id': teacherId
				}
			})
		}
	}

	const classDuration = (startDate, endDate) => {

		let start = moment(startDate);
		let end = moment(endDate);

		let duration = moment.utc(moment(end, "DD/MM/YYYY HH:mm:ss").diff(moment(start, "DD/MM/YYYY HH:mm:ss"))).format("HH:mm");

		// console.log("Difference of time ==", duration);
		return duration;

	}

	// For recomended class details/ redirect - 
	const viewClassDetails = (classId, sessionId, status) => {

		history.push({
			pathname: '/classdetails/' + `${sessionId}`,
			state: {
				'session': sessionId,
				'class': classId,
				'status': status ? status : null
			}
		})

	}

	// To sign up a recomended class/ redirect - 
	const enrollClass = async (classId, sessionId) => {

		if (userId) {
			setLoading(true);
			let dataToSend = {
				userId: userId,
				classId: classId,
				sessionId: sessionId
			}

			let path = `${API_BASEURL}/class/signup-session-by-student`;
			let res = await post(path, dataToSend);

			if (res && res.serverResponse && !res.serverResponse.isError) {
				setLoading(false);
				Swal.fire("Success", "Successfully enrolled", "Success");
				getRecomendClass();
				getMyClassList();
			} else {
				setLoading(false);
				Swal.fire("Error", "Sorry! Try again later", "Error");
			}
		} else {
			Swal.fire("Error", "Sorry! You are not logged in", "Error");
		}

	}

	// To view my class details / redirect- 
	const viewJoinLink = (classId, sessionId) => {

		history.push({
			pathname: '/joinclass/' + `${sessionId}`,
			state: {
				'session': sessionId,
				'class': classId
			}
		})

	}

	// For canceling session - 
	const cancelLink = async (classId, sessionId) => {

		setLoading(true);
		let dataToSend = {
			userId: userId,
			sessionId: sessionId
		}

		let path = `${API_BASEURL}/class/cancel-session-by-student`;
		let res = await post(path, dataToSend);

		if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
			Swal.fire("Success", "Successfully Canceled", "Success");
			setLoading(false);
			getMyClassList(userId);
		} else {
			Swal.fire("Error", res.serverResponse.message, "Error");
			setLoading(false);
		}


	}


	return (
		<>
			<Header />
			<section class="my-classes for-find-class">

				<SubHeader tag={'class'} />
				<div class="container-fluid" style={{ marginTop: '110px' }}>
					<div class="heading">
						{/* <h2>Recommended classes</h2> */}
						<h2>My classes</h2>
					</div>
				</div>

				<>
					{/* For showing session list- */}
					{myClass && myClass.length > 0 && myClass.map((value, index) => {
						return <div class="container" key={index}>
							<div class="heading">
								{/* <h2>{moment(value.date).format('ll')}</h2> */}
								<h4>{moment(value.date, "YYYY-MM-DD HH:mm:ss").format('dddd')}</h4>
							</div>

							<div class="row">
								{value.session && value.session.length > 0 && value.session.map((data, key) => {
									return <div class="col-lg-12" key={key}>
										<div class="outer-sec row">
											<div class="figure">
												<img src={data.bannerImage && data.bannerImage} alt="" />
											</div>

											<div class="figcaption">
												<h5>{data.name ? data.name : 'Class'}</h5>

												<span class="d-block">
													{data.ageGroupName ? data.ageGroupName : 'Age Group'} &nbsp;&nbsp; | &nbsp;&nbsp;
													{data.categoryName ? data.categoryName : 'Category'} &nbsp;&nbsp; | &nbsp;&nbsp;
													{value.date ? moment(value.date).format('DD-MM-YYYY') : 'Start Date'} &nbsp;&nbsp; | &nbsp;&nbsp;
													{/* {data.startTime && data.endTime ? `${data.startTime}` + ' - ' + `${data.endTime}` : ''} &nbsp;&nbsp; | &nbsp;&nbsp; */}
													{/* {data.startTime ? moment(data.startTime).format('hh.mm a') : 'Start Time'} &nbsp;&nbsp; | &nbsp;&nbsp;
													{data.startTime && data.endTime && classDuration(data.startTime, data.endTime) + ' Hrs'} &nbsp;&nbsp; | &nbsp;&nbsp;

													{data.status && data.isActive && data.status == "upcoming" ?
														<span style={{ color: 'blue' }}>Upcomming</span>
														: data.status && data.isActive && data.status == "cancelled" ?
															<span style={{ color: 'red' }}>Cancelled</span>
															: data.status && data.isActive && data.status == "ongoing" ?
																<span style={{ color: 'green' }}>Ongoing</span>
																: <span>{data.status && data.status}</span>} */}

												</span>

												<p className="">
													{data.startTime ? moment(data.startTime).format('hh.mm a') : 'Start Time'} &nbsp;
													{data.startTime && data.endTime && classDuration(data.startTime, data.endTime) + ' Hrs'} &nbsp;
												{data.status && data.isActive && data.status == "upcoming" ?
														<span style={{ color: 'blue' }}>Upcomming</span>
														: data.status && data.isActive && data.status == "cancelled" ?
															<span style={{ color: 'red' }}>Cancelled</span>
															: data.status && data.isActive && data.status == "ongoing" ?
																<span style={{ color: 'green' }}>Ongoing</span>
																: <span>{data.status && data.status}</span>}

												</p>

												<p class="d-block">
													<p onClick={() => goToTeacherProfile(data.teacherId && data.teacherId)} style={{ cursor: 'pointer' }}>
														{data.teacherName && data.teacherName}
													</p>
												</p>
											</div>

											<div class="buttons">

												<a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => viewClassDetails(data.classId, data.sessionId, true)}>Details</a>
												{data.status && data.isActive && data.status == "upcoming" || data.status == "ongoing" ?
													// <a href="JavaScript:Void(0);" class="nav-link btn btn-head signup" onClick={() => viewJoinLink(value.classId, value.sessionId)}>Join Now</a>
													<>
														<a href="javaScript:Void(0);" class="nav-link btn btn-head signup" onClick={() => viewJoinLink(data.classId, data.sessionId)}>
															Start Class
                                                	</a>
														{/* <a href="javaScript:Void(0);" class="nav-link btn btn-head signup" onClick={() => cancelLink(data.classId, data.sessionId)}>
														Cancel
													</a> */}
													</>
													: null}

											</div>

										</div>
									</div>
								})}
							</div>
						</div>

					})}
				</>

				<div class="row">
					{/* {myClass && myClass.length > 0 && myClass.map((data, index) => {
							 
							return data.session.length> 0 && data.session.map((value, key)=>{
								return<div class="col-lg-4" key={key}>
									<div class="outer-sec row">
										<div class="figure">
											<img src={value.bannerImage && value.bannerImage} alt="" />
										</div>

										<div class="figcaption">
											<h5>{value.name && value.name}</h5>
											<strong>{data.date && moment(data.date, "YYYY-MM-DD HH:mm:ss").format('dddd')}</strong>
											
											<span class="d-block">{value.dateOfConduct && moment(value.dateOfConduct).format('ll')} &nbsp;&nbsp; | &nbsp;&nbsp; {value.startTime && value.startTime}</span>
											
											<p class="d-block">
												{value.teacherName && value.teacherName}<br />
												
												{value.status && value.isActive && value.status == "upcoming" ?
													<span style={{ color: 'blue' }}>Upcomming</span>
													: value.status && value.isActive && value.status == "cancelled" ?
														<span style={{ color: 'red' }}>Cancelled</span>
														: value.status && value.isActive && value.status == "ongoing" ?
															<span style={{ color: 'green' }}>Ongoing</span>
															: <span>{value.status && value.status}</span>}
											</p>
											<a href="JavaScript:Void(0);" onClick={() => viewClassDetails(value.classId, value.sessionId)}>Details</a>
											{value.status && value.isActive && value.status == "upcoming" || value.status == "ongoing" ?
												<a href="JavaScript:Void(0);" class="joinnow" onClick={() => viewJoinLink(value.classId, value.sessionId)}>Join Now</a>
												: null}
										</div>
									</div>
								</div>
							})
						
						})} */}
				</div>

			</section>

			<section class="my-classes for-user-dashboard-page">
				<div class="container-fluid">
					<div class="heading">
						{/* <h2>My classes</h2> */}
						<h2>Recommended classes</h2>
					</div>

					<div class="row">
						{recomendClass && recomendClass.length > 0 && recomendClass.map((data, index) => {
							return <div class="col-lg-3" key={index}>
								<div class="price_item">
									<div class="figure">
										<img src={data.bannerImage && data.bannerImage} alt="" />
									</div>
									<div class="figcaption">
										<h5 class="text-uppercase">{data.name && data.name}</h5>
										<strong>{moment(data.dateOfConduct).format('dddd')} </strong>
										<span class="d-block">{moment(data.dateOfConduct).format('ll')}</span>
										<span class="d-block">{data.startTime && moment(data.startTime).format('hh.mm a')} &nbsp;&nbsp; | &nbsp;&nbsp; {data.startTime && data.endTime && classDuration(data.startTime, data.endTime)}</span>
										<p class="d-block">{data.teacherName && data.teacherName}</p>
										<a href="JavaScript:Void(0);" onClick={() => viewClassDetails(data.classId, data.sessionId)}>Details</a>
										{!isEnrolled(data.enrolledStudent) ?
											<a href="JavaScript:Void(0);" class="signup" onClick={() => enrollClass(data.classId, data.sessionId)}>Enroll</a>
											: null}
									</div>
								</div>
							</div>
						})}


					</div>


				</div>
			</section>

			<Footer />
		</>
	)

}
const condition = authUser => authUser && authUser.userType === SG_STUDENT;
export default withAuthorization(condition)(UserDashboard);