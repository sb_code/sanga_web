import React, { Component } from 'react';



class AppInfo extends Component {
    render() {
        return (
            <>
                <section class="Coming-soon">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="figcaption">
                                    <h3>
                                        Mobile App<br />
                                Coming Soon
                            </h3>
                                    <p>Your kids can wake up and workout, then learn geography, science, arts and so much more and end the day with a mindful yoga session.</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="figure">
                                    <img src="images/mob.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}

export default AppInfo;