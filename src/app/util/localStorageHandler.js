export function setData(data) {
  try {
    localStorage.setItem("SANGA_Local", JSON.stringify(data));
    return true
  } catch (error) {
    return null
  }
}

export function getData() {
  try {
    let storage = localStorage.getItem("SANGA_Local");
    storage = storage ? JSON.parse(storage) : null;
    // console.log('storage',storage);
    return storage;
  } catch (error) {
    return null
  }
}