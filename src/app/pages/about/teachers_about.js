import React, { useState, useEffect } from 'react';
import Header from '../layout/header.js';
import SubHeader from '../layout/subheader.js';
import Footer from '../layout/footer.js';


function TeacherAbout(){
    
    return (
        <>
            <Header />

            <section class="msg-box">
                
                <SubHeader tag={'about'}/>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="video-sec teacher-about">
                                <iframe width="100%" height="350" src="https://www.youtube.com/embed/xcJtL7QggTI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="login-form teacher-about">
                                <form>
                                    <input type="text" class="form-control" placeholder="Enter your fullname" />
                                    <textarea type="text" class="form-control" placeholder="Write about here..."></textarea>

                                    <button type="button" class="btn signin">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <Footer />
        </>
    )
    
}

export default TeacherAbout;