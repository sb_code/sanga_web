import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { logout, setSearch, selectUser } from '../../../actions/user/userSlice';
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import AuthHeader from '../layout/header.js';
import AuthFooter from '../layout/footer.js';
import { setData, getData } from '../../util/localStorageHandler.js';



function FindClass() {
    const user = useSelector(selectUser);
    const dispatch = useDispatch();

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [searchKey, setSearchKey] = useState('');
    const [searching, setSearching] = useState(false);
    const [userId, setUserId] = useState('');
    const [userType, setUserType] = useState('');
    const [categoryList, setCategoryList] = useState([]);
    const [ageGroupList, setAgeGroupList] = useState([]);
    const [filter, setFilter] = useState({ 'category': '', 'ageGroup': '', 'fromDate': '', 'toDate': '', 'time': '' });
    const [dateSelector, setDateSelector] = useState('all');
    const [sessionList, setSessionList] = useState([]);

    useEffect(() => {
        let data = getData();
        if (data) {
            // internalLogin(data.userType);
            setUserId(data.userId);
            setUserType(data.userType);
        } else {
            setUserId('');
        }
        let stateValue = history.location.state && history.location.state.filter;
        // let stateSearch = history.location.state && history.location.state.search;

        if (stateValue) {
            setFilter(stateValue);
            getSession(stateValue);
        } else if (user.searching) {
            searchSession(user.searchKey);
            setSearchKey(user.searchKey);
            setSearching(user.searching);
        } else {
            getSession();
        }
        getCategory();
        getAgeGroup();

    }, []);


    useEffect(() => {

        if (user.searching) {
            // searchSession(user.searchKey);
            setSearchKey(user.searchKey);
            setSearching(user.searching);
        } else {
            setSearchKey('');
            setSearching(false);
            getSession();
        }

    }, [user, setSearchKey, setSearching])


    const internalLogin = (userType) => {
        // console.log('here 3', userType);

        if (userType == SG_STUDENT) {
            history.push("/userdashboard");
        } else if (userType == SG_TEACHER) {
            history.push("/teachersdashboard");
        } else {
            Swal.fire("Error", 'userType not found', "error");
        }
    }

    // getting category list - 
    const getCategory = async () => {
        setLoading(true);

        let dataToSend = {};
        let path = `${API_BASEURL}/class/find-category`;

        let res = await post(path, dataToSend);
        // console.log("Response from get category ===", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setCategoryList(res.result && res.result)
            setLoading(false);
        } else {
            setCategoryList("");
            setLoading(false);
        }

    }

    // getting age group list --
    const getAgeGroup = async () => {
        setLoading(true);
        let dataToSend = {};
        let path = `${API_BASEURL}/class/find-age-group`;

        let res = await post(path, dataToSend);
        // console.log("Response from get age group ===", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setAgeGroupList(res.result && res.result);
            setLoading(false);
        } else {
            setAgeGroupList("");
            setLoading(false);
        }
    }

    //getting sessions -- 
    const getSession = async (sendFilter) => {
        // console.log("filter object ===", filter);
        setLoading(true);
        setSearching(false);
        setSearchKey('');
        let payload = {
            "searchKey": '',
            "searching": false
        };
        dispatch(setSearch(payload));

        let dataToSend = {}
        if (sendFilter) {
            dataToSend = {
                "page": '',
                "limit": '',
                "categoryId": sendFilter.category,
                "ageGroupId": sendFilter.ageGroup,
                "startDate": sendFilter.fromDate,
                "endDate": sendFilter.toDate
            };
        } else {
            dataToSend = {
                "page": '',
                "limit": '',
                "categoryId": filter.category,
                "ageGroupId": filter.ageGroup,
                "startDate": filter.fromDate,
                "endDate": filter.toDate
            };
        }

        let path = `${API_BASEURL}/class/get-session`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError) {
            if (res.serverResponse.statuscode != 200) {
                // Swal.fire("Error", res.serverResponse.message, "Error");
                setSessionList([]);
                setLoading(false);
            } else {
                setSessionList(res.result);
                setLoading(false);
            }
            // console.log("Response from APi -- sessions //", sessionList);
        } else {
            setLoading(false);
            setSessionList([]);
        }

    }

    // to get searched session - 
    const searchSession = async (search) => {

        setLoading(true);
        let dataToSend = {
            "searchItem": search,
            "page": '',
            "limit": ''
        }

        let path = `${API_BASEURL}/class/get-session-by-class-or-category`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && res.serverResponse.isError && res.serverResponse.statuscode != 200) {

            // Swal.fire("Error", res.serverResponse.message, "Error");
            setLoading(false);
            setSessionList([]);
            // console.log("Response from APi -- sessions //", sessionList);
        } else {
            setLoading(false);
            setSessionList(res.result);
            setLoading(false);
        }

    }

    const filterByCategory = (categoryId) => {
        filter.category = categoryId;
        setFilter(filter);
        getSession();

    }

    const filterByAgeGroup = (ageGroupId) => {

        filter.ageGroup = ageGroupId;
        setFilter(filter);
        getSession();

    }

    const filterByWeek = () => {
        setDateSelector('week');
        let currentDate = moment().format("YYYY-MM-DD");
        let currentDayNo = moment(currentDate).day();
        // console.log(" current day no is == ", currentDate);
        let firstWeekDay = moment(currentDate).subtract(currentDayNo, 'days').format("YYYY-MM-DD");
        let lastWeekDay = moment(currentDate).add(6 - currentDayNo, 'days').format("YYYY-MM-DD");

        filter.fromDate = firstWeekDay;
        filter.toDate = lastWeekDay;
        setFilter(filter);
        getSession();

        // console.log("first date == ", firstWeekDay);
        // console.log("last date == ", lastWeekDay);
    }

    const filterByDate = (event, name) => {
        event.preventDefault();
        let value = event.target.value;

        if (name == "from") {
            filter.fromDate = value;
            setFilter(filter);
            setDateSelector('custom');
            if (filter.toDate != "") { getSession(); }
        } else if (name == 'to') {
            filter.toDate = value;
            setFilter(filter);
            setDateSelector('custom');
            if (filter.fromDate != "") { getSession(); }
        } else if (name == 'all') {
            filter.fromDate = '';
            filter.toDate = '';
            setFilter(filter);
            setDateSelector('all');
            getSession();
        }

    }

    const customFilter = () => {
        filter.fromDate = '';
        filter.toDate = '';
        setFilter(filter);
        setDateSelector('custom');
    }

    const isEnrolled = (enrolledStudent) => {

        if (enrolledStudent && enrolledStudent.length > 0) {
            for (let i = 0; i < enrolledStudent.length; i++) {
                if (enrolledStudent[i].userId == userId) {
                    return true
                }
            }
        } else {
            return false;
        }

    }

    const classDuration = (startDate, endDate) => {

        let start = moment(startDate);
        let end = moment(endDate);
        // let duration = endDate.diff(startDate, 'hours');
        // let duration = moment(moment.duration(end.diff(start))).format("hh:mm");

        let duration = moment.utc(moment(end, "DD/MM/YYYY HH:mm:ss").diff(moment(start, "DD/MM/YYYY HH:mm:ss"))).format("HH:mm");

        // console.log("Difference of time ==", duration);
        return duration;

    }

    // For showing any session details - 
    const viewSessionDetails = (classId, sessionId, status) => {

        // history.push('/classdetails/'+sessionId);
        history.push({
            pathname: '/classdetails/' + `${sessionId}`,
            state: {
                'session': sessionId,
                'class': classId,
                'status': status? status: null
            }
        })

    }

    // For showing teacher's profile -
    const goToTeacherProfile = (teacherId) => {
        if (teacherId) {
            history.push({
                pathname: "/teacherprofile/" + `${teacherId}`,
                state: {
                    'id': teacherId
                }
            })
        }
    }

    // For enrolling to a session -
    const enrollSession = async (classId, sessionId) => {
        if (userId) {
            setLoading(true);
            let dataToSend = {
                userId: userId,
                classId: classId,
                sessionId: sessionId
            }

            let path = `${API_BASEURL}/class/signup-session-by-student`;
            let res = await post(path, dataToSend);

            if (res && res.serverResponse && !res.serverResponse.isError) {
                setLoading(false);
                Swal.fire("Success", "Successfully enrolled", "Success");

                if (userType == SG_STUDENT) {
                    history.push({
                        pathname: '/userdashboard',
                        state: {}
                    })
                } else {
                    getSession();
                }
            } else {
                setLoading(false);
                Swal.fire("Error", res.serverResponse.message, "Error");
            }
        } else {
            Swal.fire("Error", "Sorry! You are not logged in", "Error");
        }

    }

    // who are not logged in--
    const signIn = (classId, sessionId) => {

        // history.push({
        //     pathname: '/signin',
        //     state: {}
        // })

        window.location.replace('/signin');

    }

    const joinToSession = (classId, sessionId) => {

        history.push({
            pathname: '/joinclass/' + `${sessionId}`,
            state: {
                'session': sessionId,
                'class': classId
            }
        })

    }
    // console.log("loaded category list is ==", categoryList);

    return (
        <>
            <AuthHeader />
            <section class="my-classes for-find-class">
                <div class="active-heading find-class-one">
                    <div class="container">
                        <ul>
                            <li class={filter.category === "" ? "active" : ""} onClick={() => filterByCategory('')} style={{ cursor: "pointer" }}>All</li>
                            {categoryList && categoryList.map((data, index) => {
                                return <li onClick={() => filterByCategory(data.categoryId)} key={index}
                                    class={filter.category && filter.category == data.categoryId ? "active" : ""} style={{ cursor: "pointer" }}
                                >
                                    {data.categoryName}
                                </li>
                            })}
                            {/* <li>Arts</li>
                            <li>Cooking</li>
                            <li>Davce</li>
                            <li>Music</li>
                            <li>Languages</li>
                            <li>STEM</li>
                            <li>Yoga</li> */}
                        </ul>
                    </div>
                </div>

                <div class="active-heading find-class-one next-part">
                    <div class="container">
                        <ul>
                            <li class={filter.ageGroup === "" ? "active" : ""} onClick={() => filterByAgeGroup('')} style={{ cursor: "pointer" }}>All Ages</li>
                            {ageGroupList && ageGroupList.map((data, index) => {
                                return <li key={index} onClick={() => filterByAgeGroup(data.ageGroupId)}
                                    class={filter.ageGroup === data.ageGroupId ? "active" : ""} style={{ cursor: "pointer" }} >
                                    {data.ageGroupName}
                                </li>
                            })}
                            {/* <li>4-5 Years</li>
                            <li>5-7 Years</li>
                            <li>7-9 Years</li>
                            <li>9-11 Years</li> */}
                        </ul>
                    </div>
                </div>

                <div class="active-heading find-class-one custom">
                    <div class="container-fluid">
                        <ul>
                            <li class={dateSelector == 'all' ? "active" : ""} style={{ cursor: "pointer" }} onClick={(e) => filterByDate(e, 'all')}>
                                All
                            </li>
                            <li onClick={() => filterByWeek()} style={{ cursor: "pointer" }} class={dateSelector == 'week' ? "active" : ""} >
                                This Week
                            </li>
                            <li class={dateSelector == 'custom' ? "active" : ""} onClick={() => customFilter()} style={{ cursor: "pointer" }}>
                                Custom:
                            </li>
                            <li class="d-flex">
                                <strong>From Date:</strong>
                                <form class="d-flex">
                                    <input type="date" class="form-control" placeholder="Select form date" onChange={(e) => filterByDate(e, 'from')}
                                        readOnly={dateSelector == 'custom' ? false : true} />
                                </form>
                                <strong>To Date:</strong>
                                <form class="d-flex">
                                    <input type="date" class="form-control" placeholder="Select to date" onChange={(e) => filterByDate(e, 'to')}
                                        readOnly={dateSelector == 'custom' ? false : true} />
                                    {/* <input type="time" class="form-control" placeholder="Select time" /> */}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
                {!loading && !sessionList || sessionList.length == 0 ?
                    <div class="container" >
                        <h5 style={{ textAlign: 'center' }}>No data found</h5>
                    </div>
                    : null}
                {/* For showing session list- */}
                {sessionList && sessionList.length > 0 && sessionList.map((value, index) => {
                    return <div class="container" key={index}>
                        <div class="heading">
                            {/* <h2>{moment(value.date).format('ll')}</h2> */}
                            <h2>{moment(value.date, "YYYY-MM-DD HH:mm:ss").format('dddd')}</h2>
                        </div>

                        <div class="row">
                            {value.session && value.session.length > 0 && value.session.map((data, key) => {
                                return <div class="col-lg-12" key={key}>
                                    <div class="outer-sec row">
                                        <div class="figure">
                                            <img src={data.bannerImage && data.bannerImage} alt="" />
                                        </div>

                                        <div class="figcaption">
                                            <h5>{data.name ? data.name : 'Class'}</h5>

                                            <span class="d-block">
                                                {data.ageGroupName ? data.ageGroupName : 'Age Group'} &nbsp;&nbsp; | &nbsp;&nbsp;
                                                {data.categoryName ? data.categoryName : 'Category'} &nbsp;&nbsp; | &nbsp;&nbsp;
                                                {value.date ? moment(value.date).format('DD-MM-YYYY') : 'Start Date'} &nbsp;&nbsp; | &nbsp;&nbsp;
                                                {data.startTime ? moment(data.startTime).format('hh.mm a') : 'Start Time'} &nbsp;&nbsp; | &nbsp;&nbsp;
                                                {data.startTime && data.endTime && classDuration(data.startTime, data.endTime)+' Hrs'}
                                            </span>
                                            <p class="d-block">
                                                <p onClick={() => goToTeacherProfile(data.teacherId && data.teacherId)} style={{ cursor: 'pointer' }}>
                                                    {data.teacherName && data.teacherName}
                                                </p>
                                            </p>
                                        </div>

                                        <div class="buttons">
                                            <a href="javaScript:Void(0);" class="nav-link btn btn-head" 
                                                onClick={() => userId && isEnrolled(data.enrolledStudent) ? viewSessionDetails(data.classId, data.sessionId, true): viewSessionDetails(data.classId, data.sessionId, false)
                                                }
                                            >
                                                Details
                                            </a>
                                            {/* user not logged in */}
                                            {!userId && !isEnrolled(data.enrolledStudent) ?
                                                <a href="javaScript:Void(0);" class="nav-link btn btn-head signup" onClick={() => signIn(data.classId, data.sessionId)}>
                                                    Sign me up
                                                </a>
                                                : null}
                                            {/* user logged in but not enrolled */}
                                            {userId && !isEnrolled(data.enrolledStudent) ?
                                                <a href="javaScript:Void(0);" class="nav-link btn btn-head signup" onClick={() => enrollSession(data.classId, data.sessionId)}>
                                                    Enroll
                                                </a>
                                                : null}
                                            {/* user logged in and already enrolled */}
                                            {userId && isEnrolled(data.enrolledStudent) ?
                                                <a href="javaScript:Void(0);" class="nav-link btn btn-head signup" onClick={() => joinToSession(data.classId, data.sessionId)}>
                                                    Start Class
                                                </a>
                                                : null}
                                        </div>

                                    </div>
                                </div>
                            })}
                        </div>
                    </div>

                })}

                <div class="container">
                    
                </div>

            </section>

            <AuthFooter />
        </>
    )

}

export default FindClass;
