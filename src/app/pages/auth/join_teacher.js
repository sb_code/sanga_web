import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import ProgressBar from 'react-bootstrap/ProgressBar';
import { useSelector, useDispatch } from 'react-redux';
import axios from "axios";
import Swal from 'sweetalert2';
import Header from '../layout/header.js';
import Footer from '../layout/footer.js';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setUser, selectUser } from '../../../actions/user/userSlice.js';
import { setData, getData } from '../../util/localStorageHandler.js';


function JoinTeacher() {

	let history = useHistory();

	let [loading, setLoading] = useState(false);
	let [name, setName] = useState("");
	let [email, setEmail] = useState("");
	let [subject, setSubject] = useState("");
	let [location, setLocation] = useState("");
	let [uploaded, setUploaded] = useState("");
	let [url, setUrl] = useState("");

	let [nameError, setNameError] = useState("");
	let [emailError, setEmailError] = useState("");
	let [subjectError, setSubjectError] = useState("");
	let [locationError, setLocationError] = useState("");
	let [urlError, setUrlError] = useState("");

	const videoRef = React.createRef(null);

	useEffect(() => {
		let data = getData();
		if (data) {
			// console.log('here 1');
			internalLogin(data.userType);
		} else {
			// console.log('here 2');
		}
	}, []);

	const internalLogin = (userType) => {
		console.log('here 3', userType);

		if (userType == SG_STUDENT) {
			history.push("/userdashboard");
		} else if (userType == SG_TEACHER) {
			history.push("/teachersdashboard");
		} else {
			Swal.fire("Error", 'userType not found', "error");
		}
	}

	const onFileChange = (e) => {

		// let fileName = e.target.name;
		let file = videoRef.current && videoRef.current.files && videoRef.current.files[0];

		console.log("selected file is = ", file);

		let blnValid = false;
		let sizeValid = false;
		//For checking  file size ---
		let sizeInByte = file.size;
		let sizeInKB = parseFloat(parseFloat(sizeInByte) / 1024).toFixed('2');
		let sFileName = file.name;

		if (file) {

			sizeInKB <= 50000 ? sizeValid = true : sizeValid = false;

			var _validFileExtensions = [".mp4"];
			for (var j = 0; j < _validFileExtensions.length; j++) {
				var sCurExtension = _validFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}

			if (!sizeValid) {
				Swal.fire('Alert!', "File upto 50Mb is allowed.", 'warning');
			} else if (!blnValid) {
				Swal.fire('Alert!', "This format is not allowed.", 'warning');
			} else {
				setLoading(true);
				var reader = new FileReader();
				var url = reader.readAsDataURL(file);
				const data = new FormData();
				data.append('file', file);

				let path = `${API_BASEURL}/utility/upload-file`;

				axios
					.post(path,
						data,
						{
							headers: { 'Content-Type': 'multipart/form-data' },
							onUploadProgress: (progressEvent) => {
								// console.log("raw upload loader ---------", progressEvent.loaded, progressEvent.total);
								let uploadPercentage = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
								setUploaded(uploadPercentage);
							}
						}
					)
					.then(serverResponse => {
						let res = serverResponse.data;
						console.log("After uploading file ---", res);

						if (res && res.serverResponse && !res.serverResponse.isError) {
							setLoading(true);
							let url = res.result && res.result.fileUrl; 
							setUrl(url);
							Swal.fire("success", "Successfully uploaded", "success");
						} else {
							setLoading(true);
							Swal.fire('Alert!', res && res.serverResponse && res.serverResponse.message, 'warning');
						}

					})
					.catch(error => {
						console.log(error);
					})

			}


		}


	}

	const teacherRequest = async (e) => {

		e.preventDefault();

		!name ? setNameError("Give your name") : setNameError("");
		!email ? setEmailError("Give your mail") : setEmailError("");
		!subject ? setSubjectError("Give your subject") : setSubjectError("");
		!location ? setLocationError("Give a location") : setLocationError("");
		!url ? setUrlError("Uploade your video") : setUrlError("");

		if (name && email && subject && location) {
			setLoading(true);
			let dataToSend = {
				"name": name,
				"subject": subject,
				"email": email,
				"location": location,
				"videoURL": url,
			};
			let path = `${API_BASEURL}/user/request-for-join-as-teacher`;

			try {
				let response = await post(path, dataToSend);
				if (response.serverResponse.isError) {
					Swal.fire('Alert!', response.serverResponse.message, 'warning');
				} else {
					Swal.fire("success", response.serverResponse.message, "success");
					history.push("/");
				}


			} catch (error) {
				console.log(error);
				Swal.fire('Error!', error.message, 'error')
			}

		}


	}


	return (
		<>
			<Header />
			<section class="join-teacher-top text-center">
				<div class="container">
					<div class="heading">
						<h2 class="text-center">See what other teachers are saying..</h2>
					</div>
					<div class="login-form">
						<div class="star-sec">
							<span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></span>
						</div>
						<p>Sanga is amazing, they treat the teachers really well! I absolutely love teaching for Sanga! I am earning 2x more by working 2x less!</p>
						<div class="row">
							<div class="col-lg-6">
								<iframe width="100%" height="300" src="https://www.youtube.com/embed/xcJtL7QggTI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
							<div class="col-lg-6">
								<iframe width="100%" height="300" src="https://www.youtube.com/embed/xcJtL7QggTI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="login sec_pad join-teacher">
				<div class="container">
					<div class="heading">
						<h2 class="text-center">Join as a Teacher</h2>
					</div>
					<div class="login-form">
						<form>
							<input type="text" class="form-control" placeholder="Enter your full name" value={name} onChange={e => {setName(e.target.value); setNameError("");}} />
							{nameError && <h8 class="error-text">{nameError}</h8>}

							<input type="text" class="form-control" placeholder="Enter your email address" value={email} onChange={e => {setEmail(e.target.value); setEmailError("");}} />
							{emailError && <h8 class="error-text">{emailError}</h8>}

							{/* <input type="number" class="form-control" placeholder="Enter your country code" value={countryCode} onChange={e => setCountryCode(e.target.value)} />
                            {countryCodeError && <h8 class="error-text">{countryCodeError}</h8>} */}

							<input type="text" class="form-control" placeholder="Enter your Subject" value={subject} onChange={e => {setSubject(e.target.value); setSubjectError("");}} />
							{subjectError && <h8 class="error-text">{subjectError}</h8>}

							<input type="text" class="form-control" placeholder="Enter your location" value={location} onChange={e => {setLocation(e.target.value);setLocationError("");}} />
							{locationError && <h8 class="error-text">{locationError}</h8>}

							<input type="text" class="form-control" placeholder="Enter youtube url" value={url} onChange={e => {setUrl(e.target.value); setUrlError("");}} />
							{urlError && <h8 class="error-text">{urlError}</h8>}

							<p>OR Submit a 1-3 minute video (50 MB max)</p>
							<p class="d-block">
								<input type="file" class="upload" id="myFile"
									name="video" ref={videoRef}
									onChange={(e) => { onFileChange(e) }} multiple={false}
								/>
							</p>
							{uploaded ?
								<ProgressBar striped variant="success" now={uploaded} label={`${uploaded}%`} />
								:
								null}
							<br />

							<button type="button" class="btn signin" onClick={e => teacherRequest(e)}>Send Request</button>
						</form>
					</div>
				</div>
			</section>
			<Footer />
		</>
	);

}

export default JoinTeacher;