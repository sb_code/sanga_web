import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";

import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';

import Header from '../layout/header.js';
import SubHeader from '../layout/subheader.js';
import Footer from '../layout/footer.js';

import { setData, getData } from '../../util/localStorageHandler.js';


function UserAbout() {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [edit, setEdit] = useState(false);
    const [userId, setUserId] = useState('');
    const [userDetails, setUserDetails] = useState({});
    const [userType, setUserType] = useState('');
    const [name, setName] = useState('');
    const [dob, setDob] = useState('');
    const [favourite, setFavourite] = useState('');
    const [food, setFood] = useState('');


    useEffect(() => {
        let data = getData();
        if (data) {
            setUserId(data.userId);
            setUserType(data.userType);
            getUserDetails(data.userId);
        }
    }, [])


    const getUserDetails = async (userId) => {

        let dataToSend = {
            "userId": userId
        }
        setLoading(true);
        let path = `${API_BASEURL}/user/get-user-by-id`;
        let res = await post(path, dataToSend);

        if (res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setUserDetails(res.result);
            setLoading(false);
            setName(res.result.name);
            setDob(res.result.dob);
            setFavourite(res.result.favourite);
            setFood(res.result.name);
        } else {
            setLoading(false);
        }

    }

    const updateProfile = async (e) => {
        e.preventDefault();
        if (!loading) {
            if (userType == SG_STUDENT) {
                let dataToSend = {
                    "name": name,
                    "userId": userId,
                    "dob": dob,
                    "favourite": favourite
                }

                setLoading(true);
                let path = `${API_BASEURL}/user/update-student`;
                let res = await post(path, dataToSend);

                if (res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
                    setLoading(false);
                    getUserDetails(userId);
                    Swal.fire("Success!", "Successfully updated.", "success");
                } else {
                    setLoading(false);
                    res && res.serverResponse && Swal.fire("Error!", res.serverResponse.message, "error");
                }
            }
        }
    }


    return (
        <>
            <Header />
            <section class="login for-user-about pb-4">

                <SubHeader tag={'about'} />

                <div class="container">
                    <br /><br /><br /><br />
                    {/* <div class="heading">
                        <h2 class="text-center"><img src="images/user2.png" /></h2>
                    </div> */}
                    {/* {!edit ?
                        <div>
                            <span style={{ color: "#03989e", cursor: "pointer" }} onClick={() => setEdit(true)}>Edit Profile</span>
                        </div>
                        : null} */}

                    <div class="login-form">
                        <form>
                            <label>Name:</label>
                            <input type="text" class="form-control"
                                placeholder="Enter your full name"
                                value={name} onChange={(e) => setName(e.target.value)}
                            />
                            <label>Credit:</label>
                            <input type="text" class="form-control"
                                placeholder="Your Credit" value={userDetails.classCredit} readOnly={true} />
                            <label>DOB:</label>
                            <input type="date" class="form-control"
                                placeholder="Enter your date of birth"
                                value={dob} onChange={(e) => setDob(e.target.value)}
                            />
                            <label>Favourite Things:</label>
                            <input type="text" class="form-control"
                                placeholder="Enter favorite things to do"
                                value={favourite} onChange={(e) => setFavourite(e.target.value)}
                            />
                            {/* <input type="text" class="form-control" 
                                placeholder="Enter your favorite food" 
                                value={food} onChange={(e)=>setFood(e.target.value)}
                            /> */}
                            <button type="button" class="btn signin" onClick={(e) => updateProfile(e)}>Save</button>

                        </form>
                    </div>
                </div>
            </section>
            <Footer />
        </>
    )

}

export default UserAbout;