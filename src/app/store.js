import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import userReducer from '../actions/user/userSlice';
import logger from 'redux-logger'
import { reduxBatch } from '@manaflair/redux-batch'

const middleware = [...getDefaultMiddleware(), logger]

const reducer = {
  user: userReducer,
}

export default configureStore({
  reducer,
  middleware,
  enhancers: [reduxBatch],
});

/**
 * https://redux-toolkit.js.org/api/configureStore
 * https://www.npmjs.com/package/@manaflair/redux-batch
 * https://github.com/LogRocket/redux-logger#readme
 */
