import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import Swal from 'sweetalert2';
import Header from '../layout/header.js';
import Footer from '../layout/footer.js';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setUser, selectUser } from '../../../actions/user/userSlice.js';
import { setData, getData } from '../../util/localStorageHandler.js';


function ForgotPassword() {

  let history = useHistory();
  
  let [email, setEmail] = useState("");
	let [loading, setLoading] = useState(false);
	let [emailError, setEmailError] = useState("");
  

  useEffect(() => {
		let data = getData();
		if (data) {
			internalLogin(data.userType);
		}
  }, []);
  
  const internalLogin = (userType) => {
		if (userType == SG_STUDENT) {
			history.push("/userdashboard");
		} else if (userType == SG_TEACHER) {
			history.push("/teachersdashboard");
		} else {
			Swal.fire("Error", 'userType not found', "error");
		}
	}

  const sendResetMail= async()=>{

    if(email){

      setLoading(true)
      let url = `${API_BASEURL}/user/forget-password`;
      let dataToSend = {
        email
      }

      try{
				let response = await post(url, dataToSend);
        // console.log("response from forget-password===", response);
        if (response.serverResponse.isError) {
          Swal.fire('Alert!', response.serverResponse.message, 'warning')
        } else {
          if(response.serverResponse.statuscode != 200){
            Swal.fire('Alert!', "Sorry! user not found", 'warning')
						history.push("/signin");
          }else{
            Swal.fire("success", response.serverResponse.message, "success");
						history.push("/signin");
          }
        }

      } catch(error){
        console.log(error);
				Swal.fire('Error!', error.message, 'error')
      }     
			setLoading(false);
    }else{
			if (!email) setEmailError("Enter valide email.")
    }

  }

  
  
  return (
    <>
      <Header />
      <section class="login sec_pad">
        <div class="container">
          <div class="heading">
            <h2 class="text-center">Forgot Password!</h2>
          </div>
          <div class="login-form">
            <p class="text-center">Enter your registered email. We will send a password reset link to your email. By following that link you can reset your password. </p>
            <form>
              <input type="email"
                class="form-control" 
                placeholder="Enter your email" 
                value= {email}
								onChange={e => setEmail(e.target.value)}
              />
              <button type="button" 
                class="btn signin"
                onClick= {sendResetMail}
              >
                Send Reset Link
              </button>
            </form>
          </div>
        </div>
      </section>
      <Footer />
    </>
  )
  
}

export default ForgotPassword;