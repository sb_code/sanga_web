import React, { useState, useEffect } from 'react';
import Payment from './payment.js';
import {
    Elements,
    CardElement,
    useStripe,
    useElements,
  } from "@stripe/react-stripe-js";
  import { loadStripe } from "@stripe/stripe-js";


function PaymentWrap() {

    const stripeKey = loadStripe(
        "pk_test_2hBvqiD8SMEIBNkuvG1p4aQm"
      );

    return (
        <Elements stripe={stripeKey}>
            <Payment />
        </Elements>
    )

}

export default PaymentWrap;