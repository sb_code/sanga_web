import React, { Component } from 'react';


class FrontFooter extends Component {
    render() {
        return (
            <>

                <footer class="footer_area">
                    <div class="footer_top">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="social-icon">
                                        <div class="social_icon">
                                            <a href="#" class="fa fa-facebook"></a>
                                            <a href="#" class="fa fa-twitter"></a>
                                            <a href="#" class="fa fa-instagram"></a>
                                            <a href="#" class="fa fa-youtube"></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <div class="Social-link">
                                        <ul class="list-unstyled f_list">
                                            <li><a href="#">Company</a></li>
                                            <li><a href="#">Leadership</a></li>
                                            <li><a href="#">Diversity</a></li>
                                            <li><a href="#">Jobs</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer_bottom">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="mb-0 text-center">Copyright © 2020-21 <a href="#">Sanga</a>. All Rights Reserved.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </>
        )
    }
}

export default FrontFooter;