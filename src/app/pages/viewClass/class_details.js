import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';

import AuthHeader from '../layout/header.js';
import AuthFooter from '../layout/footer.js';

import { setData, getData } from '../../util/localStorageHandler.js';


function ClassDetails() {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [userId, setUserId] = useState('');
    const [userType, setUserType] = useState('');
    const [classId, setClassId] = useState('');
    const [sessionId, setSessionId] = useState('');
    const [loggedIn, setLoggedIn] = useState(false);
    const [enrolled, setEnrolled] = useState(false);
    const [details, setDetails] = useState({});
    const [session, setSession] = useState({});
    const [enrolledSessions, setEnrolledSessions] = useState([]);
    // const [isEnrollPending, setIsEnrollPending] = useState(false);

    // Component did mount - 
    useEffect(() => {
        let data = getData();
        let selectedSessionId = history.location.state && history.location.state.session;
        let selectedClassId = history.location.state && history.location.state.class;
        selectedSessionId ? setSessionId(selectedSessionId) : setSessionId('');
        selectedClassId ? setClassId(selectedClassId) : setClassId('');
        if (data) {
            // internalLogin(data.userType);
            setLoggedIn(true);
            setUserId(data.userId);
            setUserType(data.userType);
            history.location.state && history.location.state.status && setEnrolled(history.location.state.status);
            getSessionDetails(history.location.state && history.location.state.session);
            getSessionListForStudent(selectedClassId, data.userId);
        } else {
            setLoggedIn(false);
            setUserId('');
        }
        getClassDetails(selectedClassId, data);

    }, []);

    // Component did update - 
    useEffect(() => {
        setEnrolledSessions(enrolledSessions);
    }, [setEnrolledSessions, enrolledSessions])

    const internalLogin = (userType) => {
        if (userType == SG_STUDENT) {
            history.push("/userdashboard");
        } else if (userType == SG_TEACHER) {
            history.push("/teachersdashboard");
        } else {
            Swal.fire("Error", 'userType not found', "error");
        }
    }

    const getSessionDetails = async (sessionId) => {

        let dataToSend = {
            "sessionId": sessionId
        }

        let path = `${API_BASEURL}/class/get-session-by-session-id`;
        let res = await post(path, dataToSend);

        if (res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            let sessionDetails = res.result && res.result;
            setSession(sessionDetails);
            isEnrolled(sessionDetails.enrolledStudent);
            console.log("class details === ", res.result);
            // enrollPending(sessionDetails);
            setLoading(false);
        } else {
            setLoading(false);
        }

    }

    const getClassDetails = async (classId, data) => {

        setLoading(true);

        let dataToSend = {
            'classId': classId
        };
        let path = `${API_BASEURL}/class/get-class-details-by-class-id`;
        let res = await post(path, dataToSend);

        if (res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            if (data) {
                // let enrollArray = await getSessionListForStudent(classId, userId);
                // let enrolledSessions = enrollArray && enrollArray.enrolled;
                // setEnrolledSessions(enrolledSessions);
                res.result && res.result.classDetails && res.result.classDetails[0] && setDetails(res.result.classDetails[0]);
            }
            res.result && res.result.classDetails && res.result.classDetails[0] && setDetails(res.result.classDetails[0]);
            // enrollPending(res.result && res.result[0].sessionDetailS);
            setLoading(false);
        } else {
            setLoading(false);
        }

    }

    const getSessionListForStudent = async (classId, userId) => {

        let dataToSend = {
            'userId': userId,
            'classId': classId
        }
        setLoading(true);
        let path = `${API_BASEURL}/class/get-session-list-for-student-of-class`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError) {
            setEnrolledSessions(res.result && res.result.sessionIdList);
            setLoading(false);
        } else {
            setEnrolledSessions([]);
            setLoading(false);
        }

        // console.log("response from getsession list ==", res);

    }

    const enrollPending = (session) => {

        if (session) {
            // session.map((data, index) => {
            for (var i = 0; i < session.length; i++) {
                let data = session[i];
                if (data.status && data.status == 'upcoming' && !enrolledSessions.includes(data.sessionId) && compareDate(data.dateOfConduct)) {
                    
                    console.log("Successfull session is ===", data);
                    return true;
                    break;
                }
            }
            // })

        } else {
            return false;
        }

    }

    // const getEnrolledArray = (sessions, userId) => {
    //     return new Promise((resolve, reject) => {
    //         let arrayE = [];
    //         if (sessions && sessions.length > 0) {
    //             sessions.map((data, index) => {
    //                 data.enrolledStudent && data.enrolledStudent.map((value, key) => {
    //                     if (value.userId == userId) {
    //                         if(value.cancelledByStudent == true){
    //                             arrayE.push(data.sessionId);
    //                         }
    //                     }
    //                 })
    //             })
    //             resolve({
    //                 'enrolled': arrayE
    //             });
    //         } else {
    //             resolve({
    //                 'enrolled': []
    //             });
    //         }
    //     })
    // }


    // Un enroll session - 
    const unEnrollFromList = async (index) => {
        let sessions = details.sessionDetailS;
        let selectedSessionId = sessions[index] && sessions[index].sessionId;

        let dataToSend = {
            userId: userId,
            sessionId: selectedSessionId
        };
        setLoading(true);
        let path = `${API_BASEURL}/class/cancel-session-by-student`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            Swal.fire("Success", "Successfully Canceled", "Success");
            setLoading(false);
            getSessionListForStudent(classId, userId);
            getClassDetails(classId, { 'userId': userId });
        } else {
            Swal.fire("Error", res.serverResponse.message, "Error");
            setLoading(false);
        }


    }

    // Enroll a session -
    const enrollFromList = async (index) => {

        let sessions = details.sessionDetailS;
        let selectedSessionId = sessions[index] && sessions[index].sessionId;

        let dataToSend = {
            userId: userId,
            classId: classId,
            sessionId: selectedSessionId
        };

        let path = `${API_BASEURL}/class/signup-session-by-student`;
        let res = await post(path, dataToSend);
        setLoading(true);
        if (res && res.serverResponse && !res.serverResponse.isError) {
            if (res.serverResponse.statuscode == 200) {
                Swal.fire("Success", "Successfully enrolled", "Success");
                setLoading(true);
                getSessionListForStudent(classId, userId);
                getClassDetails(classId, { 'userId': userId });
            } else {
                Swal.fire("Error", res.serverResponse.message, "Error");
                setLoading(true);
            }
        } else {
            Swal.fire("Error", res.serverResponse.message, "Error");
            setLoading(true);
        }

    }

    const isEnrolled = (enrolledStudent) => {

        if (enrolledStudent && enrolledStudent.length > 0) {
            for (let i = 0; i < enrolledStudent.length; i++) {
                if (enrolledStudent[i].userId == userId) {
                    setEnrolled(true);
                }
            }
        } else {
            setEnrolled(false);
        }

    }

    const compareDate = (date) => {

        let dataDate = moment(date);
        let toDay = moment(new Date());

        if (dataDate.format('YYYY-MM-DD') > toDay.format('YYYY-MM-DD')) {
            return true;
        } else {
            return false;
        }

    }

    // Enroll session - 
    const enroll = async (status) => {

        if (status) {
            let dataToSend = {};
            if (status == 'all') {
                dataToSend = {
                    userId: userId,
                    classId: classId,
                    sessionId: ""

                }
            } else if (status == 'one') {
                dataToSend = {
                    userId: userId,
                    classId: classId,
                    sessionId: sessionId
                };
            }

            let path = `${API_BASEURL}/class/signup-session-by-student`;
            let res = await post(path, dataToSend);

            if (res && res.serverResponse && !res.serverResponse.isError) {
                if (res.serverResponse.statuscode == 200) {
                    Swal.fire("Success", "Successfully enrolled", "Success");
                    if (userType == SG_STUDENT) {
                        history.push({
                            pathname: '/userdashboard',
                            state: {}
                        })
                    }
                } else {
                    Swal.fire("Error", res.serverResponse.message, "Error");
                }
            } else {
                if (res.serverResponse && res.serverResponse.statuscode == 204) {
                    Swal.fire("Success", res.serverResponse.message, "Success");
                    history.push({
                        pathname: '/userdashboard',
                        state: {}
                    })
                } else {
                    Swal.fire("Error", res.serverResponse.message, "Error");
                }
            }

        }

    }



    return (
        <>
            <AuthHeader />
            <section class="msg-box find-class sec_pad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="heading">
                                <h2>{details.name && details.name}</h2>
                                {details.teacherName && <p>Trainer: <strong>{details.teacherName}</strong></p>}
                            </div>
                            <div class="figure">
                                {/* <img src="../images/img3.png" /> */}
                                <img src={details.bannerImage && details.bannerImage} alt="" />
                            </div>
                            <div class="col-lg-12 mt-5">
                                <p>
                                    {details.about}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="find-class-right">
                                {/* <h3>Aug 27 - Sept 30, 2020</h3> */}
                                <h3>{moment(details.startDate).format('ll')} - {moment(details.endDate).format('ll')}</h3>
                                <ul>
                                    {details.daysOfWeek && details.daysOfWeek.map((value, index) => {

                                        return value.isActive ?
                                            <li class="active" key={index}>
                                                {value.name}
                                            </li>
                                            : <li key={index}>
                                                {value.name}
                                            </li>
                                    })}
                                </ul>
                                <h3>Total {details.sessionDetailS && details.sessionDetailS.length} sessions</h3>
                                <p>
                                    {details.startTime} - {details.endTime}<br />
                                    {details.ageGroupName && details.ageGroupName}
                                </p>


                                {enrollPending(details.sessionDetailS) ?
                                    <a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => enroll('all')}>
                                        Sign up for All Sessions
                                    </a>
                                    : null}

                                {loggedIn && !enrolled ?
                                    <a href="JavaScript:Void(0);" class="nav-link btn btn-head individual" onClick={() => enroll('one')}>
                                        Individual Sessions
                                </a>
                                    : null}

                                <h3>Enrolled Sessions</h3>
                                {loggedIn && enrolledSessions.length > 0 ?
                                    <p>
                                        {details.sessionDetailS && details.sessionDetailS.map((data, index) => {
                                            return data.status && data.status == 'upcoming' && enrolledSessions.includes(data.sessionId) &&<>
                                                {moment(data.dateOfConduct).format('ll')} &nbsp;&nbsp;&nbsp;&nbsp; 
                                                {compareDate(data.dateOfConduct) &&
                                                    <a href="JavaScript:Void(0);" onClick={() => unEnrollFromList(index)}>Un Enroll</a>
                                                }
                                                <br />
                                            </>
                                        })}
                                    </p>
                                    : <p>-</p>}
                                {/* <br /> */}
                                <h3>Other Sessions</h3>
                                <p>
                                    {details.sessionDetailS && details.sessionDetailS.map((data, index) => {
                                        return data.status && data.status == 'upcoming' && !enrolledSessions.includes(data.sessionId) && compareDate(data.dateOfConduct) && <>

                                            {moment(data.dateOfConduct).format('ll')}&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="JavaScript:Void(0);" onClick={() => enrollFromList(index)}>Enroll</a>
                                            <br />
                                        </>
                                    })}
                                </p>

                            </div>
                        </div>
                        {/* <div class="col-lg-12 mt-5">
                            <p>
                                {details.about}
                            </p>
                        </div> */}
                    </div>
                </div>
            </section>
            <AuthFooter />
        </>
    )

}

export default ClassDetails;