import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';
import {
  Elements,
  CardElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";


// Importing all pages after auth -- / inside APP - 

// Sanga Pages/ APP -- 
import TeacherDashboard from './app/pages/dashboard/teachers_dashboard';
import UserDashboard from './app/pages/dashboard/user_dashboard';
import UserAbout from './app/pages/about/user_about';
import TeacherAbout from './app/pages/about/teachers_about';
import TeacherMessage from './app/pages/message/teacher_message';
import UserMessage from './app/pages/message/user_message';
import ChoosePlan from './app/pages/plans/choose_plan';
import PaymentWrap from './app/pages/plans/payment_wrap';
import withAuthentication from './app/Session/withAuthentication';
import JoinTeacher from './app/pages/auth/join_teacher';
import FindClass from './app/pages/viewClass/find_class';
import ClassDetails from './app/pages/viewClass/class_details';
import JoinClass from './app/pages/viewClass/join_class';
import FrontPage from './app/pages/start/front_page';
import TeacherProfile from './app/pages/viewClass/teacher-profile';


function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/' name='Front Page' component={FrontPage} />
        <Route path='/jointeacher' name='Join as teacher' component={JoinTeacher} />
        <Route path='/findclass' name='Find Class' component={FindClass} />
        <Route path='/classdetails/:id' name='Class Details' component={ClassDetails} />
        <Route path='/teacherprofile/:id' name='Class Details' component={TeacherProfile} />
        <Route path='/joinclass/:id' name='Class Details' component={JoinClass} />
        <Route path="/teachersdashboard" name="Teachers Dashboard" component={TeacherDashboard} />
        <Route path="/userdashboard" name="Students Dashboard" component={UserDashboard} />
        <Route path="/userabout" name="About" component={UserAbout} />
        <Route path="/teacherabout" name="About" component={TeacherAbout} />
        <Route path="/teachermessage" name="Message" component={TeacherMessage} />
        <Route path="/usermessage" name="Message" component={UserMessage} />
        <Route path="/chooseplan" name="Plan" component={ChoosePlan} />
        <Route path="/payment" name="Plan" component={PaymentWrap} />
        <Route component={FrontPage} />
      </Switch>
    </BrowserRouter>
  );
}

export default withAuthentication(App);
