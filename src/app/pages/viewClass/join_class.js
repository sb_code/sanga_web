import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';

import AuthHeader from '../layout/header.js';
import AuthFooter from '../layout/footer.js';

import { setData, getData } from '../../util/localStorageHandler.js';


function JoinClass() {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [loggedIn, setLoggedIn] = useState(false);
    const [details, setDetails] = useState({});

    useEffect(() => {
        let data = getData();
        if (data) {
            // internalLogin(data.userType);
            setLoggedIn(true);
        } else {
            setLoggedIn(false);
        }
        let sessionId = history.location.state && history.location.state.session;
        let classId = history.location.state && history.location.state.class;
        // console.log("selected session id is====", history.location.state.id);
        getClassDetails(classId);
        
    }, []);

    const internalLogin = (userType) => {
        if (userType == SG_STUDENT) {
            history.push("/userdashboard");
        } else if (userType == SG_TEACHER) {
            history.push("/teachersdashboard");
        } else {
            Swal.fire("Error", 'userType not found', "error");
        }
    }

    const getClassDetails = async (classId) => {

        // setLoading(true);
        let dataToSend = {
            'classId': classId
        };
        let path = `${API_BASEURL}/class/get-class-details-by-class-id`;
        let res = await post(path, dataToSend);

        if (res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setDetails(res.result && res.result[0]);
            console.log("class details === ", res.result);
        }

    }


    return (
        <>
            <AuthHeader />
            <section class="msg-box find-class sec_pad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="heading">
                                <h2>{details.name}</h2>
                                {details.teacherName && <p>Trainer: <strong>{details.teacherName}</strong></p>}
                            </div>
                            <div class="figure">
                                {/* <img src="../images/img3.png" /> */}
                                <img src={details.bannerImage && details.bannerImage} alt=""/>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="find-class-right">
                                {/* <h3>Aug 27 - Sept 30, 2020</h3> */}
                                <h3>{moment(details.startDate).format('ll')} - {moment(details.endDate).format('ll')}</h3>
                                <ul>
                                    {details.daysOfWeek && details.daysOfWeek.map((value, index) => {

                                        return value.isActive ?
                                            <li class="active" key={index}>
                                                {value.name}
                                            </li>
                                            : <li key={index}>
                                                {value.name}
                                            </li>
                                    })}
                                </ul>
                                <h3>Total {details.sessionDetailS && details.sessionDetailS.length} sessions</h3>
                                <p>
                                    {details.startTime} - {details.endTime}<br />
                                    {details.ageGroupName}
                                </p>
                                {details.roomLink?
                                <>
                                    <a href="JavaScript:Void(0);" target= "blank">{details.roomLink}</a>
                                </>
                                :
                                    <span>Session Not started yet.</span>
                                }
                            </div>
                        </div>
                        <div class="col-lg-12 mt-5">
                            <p>
                                {details.about}
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <AuthFooter />
        </>
    )

}

export default JoinClass;