import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import Swal from 'sweetalert2';
import Header from '../layout/header.js';
import Footer from '../layout/footer.js';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setUser, selectUser } from '../../../actions/user/userSlice.js';
import { setData, getData } from '../../util/localStorageHandler.js';
import {
  Elements,
  CardElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";


function Payment() {

  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [userId, setUserId] = useState('');
  const [planId, setPlanId] = useState('');
  const [priceId, setPriceId] = useState('');
  const [planName, setPlanName] = useState('');
  const [amount, setAmount] = useState('');
  const [mode, setMode] = useState('');
  const stripeKey = loadStripe(
    "pk_test_2hBvqiD8SMEIBNkuvG1p4aQm"
  );
  const stripe = useStripe();
  const elements = useElements();


  useEffect(() => {
    let data = getData();
    if (data) {
      data.userId && setUserId(data.userId);
      history.location.state && history.location.state.planId && setPlanId(history.location.state.planId);
      history.location.state && history.location.state.priceId && setPriceId(history.location.state.priceId);
      history.location.state && history.location.state.planName && setPlanName(history.location.state.planName);
      history.location.state && history.location.state.amount && setAmount(history.location.state.amount);
      history.location.state && history.location.state.status && setMode(history.location.state.status);

    } else {
      history.push({
        pathname: '/'
      })
    }
  }, []);

  // For Subscription - 
  const payment = async (e) => {

    e.preventDefault();

    if (!stripe || !elements) {
      console.log("stripe or element not found...");
      return;
    }
    setLoading(true);

    const cardElement = await elements.getElement('card');
    // const {card} = await stripe.elements.create('card');
    if (cardElement) {
      const { token } = await stripe.createToken(cardElement);
      if (token) {
        // console.log("Stripe token generated == ", token);
        let dataToSend = {
          "userId": userId,
          "priceId": priceId,
          "token": token.id
        };

        let path = `${API_BASEURL}/payment/make-subscription-payment`;
        let res = await post(path, dataToSend);
        if (res && res.serverResponse) {
          if (!res.serverResponse.isError) {
            history.push({
              pathname: '/chooseplan'
            })
            Swal.fire("Succes", "Successfully payment done", "Succes");
            setLoading(false);
          } else {
            Swal.fire("Error", res.serverResponse.message, "Error");
            setLoading(false);
          }
        } else {
          setLoading(false);
          Swal.fire("Error", "Sorry!try again later.", "Error");
        }

      } else {
        setLoading(false);
        Swal.fire("Error", "Sorry!try again later.", "Error");
      }

    } else {
      setLoading(false);
      Swal.fire("Error", "Sorry!try again later.", "Error");
    }


  }

  // For Direct Payment - 
  const directPay = async (e) => {

    e.preventDefault();

    if (!stripe || !elements) {
      console.log("stripe or element not found...");
      return;
    }
    setLoading(true);

    const cardElement = await elements.getElement('card');
    // const {card} = await stripe.elements.create('card');
    if (cardElement) {
      const { token } = await stripe.createToken(cardElement);
      if (token) {
        // console.log("Stripe token generated == ", token);
        let dataToSend = {
          "userId": userId,
          "priceId": priceId,
          "token": token.id
        };

        let path = `${API_BASEURL}/payment/make-direct-payment`;
        let res = await post(path, dataToSend);
        if (res && res.serverResponse) {
          if (!res.serverResponse.isError) {
            history.push({
              pathname: '/chooseplan'
            })
            Swal.fire("Succes", "Successfully payment done", "Succes");
            setLoading(false);
          } else {
            Swal.fire("Error", res.serverResponse.message, "Error");
            setLoading(false);
          }
        } else {
          setLoading(false);
          Swal.fire("Error", "Sorry!try again later.", "Error");
        }

      } else {
        setLoading(false);
        Swal.fire("Error", "Sorry!try again later.", "Error");
      }

    } else {
      setLoading(false);
      Swal.fire("Error", "Sorry!try again later.", "Error");
    }


  }


  // For update subscribtion - 
  const update = async(e)=> {

    e.preventDefault();
    if (!stripe || !elements) {
      console.log("stripe or element not found...");
      return;
    }
    setLoading(true);

    const cardElement = await elements.getElement('card');
    // const {card} = await stripe.elements.create('card');
    if (cardElement) {
      const { token } = await stripe.createToken(cardElement);
      if (token) {
        // console.log("Stripe token generated == ", token);
        let dataToSend = {
          "userId": userId,
          "priceId": priceId,
          "token": token.id
        };

        let path = `${API_BASEURL}/payment/update-subscription`;
        let res = await post(path, dataToSend);
        if (res && res.serverResponse) {
          if (!res.serverResponse.isError) {
            history.push({
              pathname: '/chooseplan'
            })
            Swal.fire("Succes", "Successfully updated", "Succes");
            setLoading(false);
          } else {
            Swal.fire("Error", res.serverResponse.message, "Error");
            setLoading(false);
          }
        } else {
          setLoading(false);
          Swal.fire("Error", "Sorry!try again later.", "Error");
        }

      } else {
        setLoading(false);
        Swal.fire("Error", "Sorry!try again later.", "Error");
      }

    } else {
      setLoading(false);
      Swal.fire("Error", "Sorry!try again later.", "Error");
    }


  }


  return (
    <>
      <Header />
      <section class="login sec_pad">
        <div class="container">
          <div class="heading">
            {mode && <h2 class="text-center">Make Your {mode}</h2>}
          </div>
          <div class="login-form">

            <form>
              <input type="text" class="form-control"
                placeholder="Name"
                value={planName}
                readOnly={true}
              />

              {/* <Elements stripe={stripeKey}> */}
              <CardElement
                className="payment"
              // options={{ style: { base: {backgroundColor: "white"}}, }}
              />
              {/* </Elements> */}
              {mode && mode == 'Subscription' ?
                <button type="button" class="btn signin"
                  onClick={e => payment(e)}
                >
                  {loading ? "Loading..." : "Pay $" + amount}
                </button>
                : mode && mode == 'Payment' ?
                  <button type="button" class="btn signin"
                    onClick={e => directPay(e)}
                  >
                    {loading ? "Loading..." : "Pay $" + amount}
                  </button>
                  : mode && mode == 'change' ?
                    <button type="button" class="btn signin"
                      onClick={e => update(e)}
                    >
                      {loading ? "Loading..." : "Pay $" + amount}
                    </button>
                    : null}
            </form>

          </div>
        </div>
      </section>
      <Footer />
    </>
  )

}

export default Payment;