import React, { Component } from 'react';



class Faq extends Component {
    render() {
        return (
            <>
                <section class="question sec_pad">
                    <div class="container">
                        <div class="heading text-center">
                            <h2>Frequently Ask Question</h2>
                            <p>Got questions? We’ve got answers. If you have some other questions, feel free to send us an email to <a href="#">contact-at-repmonk.com</a></p>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="ask-sec">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header">
                                                <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                                    How does it work ? <span><i class="fa fa-plus"></i></span>
                                                </a>
                                            </div>
                                            <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                                <div class="card-body">
                                                    We have many live classes which are led by vetted teachers. Signup and we will send you the details of our live classes. Also, our summer camp is starting soon.
                                        </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header">
                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                    What are the class timings ? <span><i class="fa fa-plus"></i></span>
                                                </a>
                                            </div>
                                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                <div class="card-body">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                                    ut aliquip ex ea commodo consequat.
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="ask-sec">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header">
                                                <a class="card-link" data-toggle="collapse" href="#collapseThree">
                                                    Are these teachers vetted ? <span><i class="fa fa-plus"></i></span>
                                                </a>
                                            </div>
                                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                <div class="card-body">
                                                    We have many live classes which are led by vetted teachers. Signup and we will send you the details of our live classes. Also, our summer camp is starting soon.
                                        </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header">
                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                    How much do you charge ? <span><i class="fa fa-plus"></i></span>
                                                </a>
                                            </div>
                                            <div id="collapseFour" class="collapse" data-parent="#accordion">
                                                <div class="card-body">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                                    ut aliquip ex ea commodo consequat.
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}

export default Faq;