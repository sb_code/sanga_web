import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { setData, getData } from '../../util/localStorageHandler.js';
import { SG_STUDENT, SG_TEACHER } from '../../constants/global';



function SubHeader(props) {

    const history = useHistory();
    const [tag, setTag] = useState('');
    const [loggedIn, setLoggedIn] = useState(false);
    const [userType, setUserType] = useState('');

    useEffect(() => {
        props && props.tag && setTag(props.tag);
        let data = getData();
        if (data) {
            setLoggedIn(true);
            setUserType(data.userType);
        }
    }, [])

    const classes = () => {

        if (loggedIn && userType == SG_STUDENT) {
            history.push({
                pathname: '/userdashboard'
            })
        } else if (loggedIn && userType == SG_TEACHER) {
            history.push({
                pathname: '/teachersdashboard'
            })
        }

    }

    const message = () => {

        if (loggedIn && userType == SG_STUDENT) {
            history.push({
                pathname: '/usermessage'
            })
        } else if (loggedIn && userType == SG_TEACHER) {
            history.push({
                pathname: '/teachermessage'
            })
        }

    }

    const about = () => {

        if (loggedIn && userType == SG_STUDENT) {
            history.push({
                pathname: '/userabout'
            })
        } else if (loggedIn && userType == SG_TEACHER) {
            history.push({
                pathname: '/teacherabout'
            })
        }

    }

    return (
        <div class="active-heading" style={{ position: 'fixed', zIndex: 99 }}>
            <ul>
                <li class={tag && tag == 'class' ? "active" : ""} onClick={() => classes()} style={{ cursor: "pointer" }}>Classes</li>
                <li class={tag && tag == 'message' ? "active" : ""} onClick={() => message()} style={{ cursor: "pointer" }}>Messages</li>
                <li class={tag && tag == 'about' ? "active" : ""} onClick={() => about()} style={{ cursor: "pointer" }}>Profile</li>
            </ul>
        </div>

    );
}

export default SubHeader;