import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import Swal from 'sweetalert2';
import Header from '../layout/header.js';
import Footer from '../layout/footer.js';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setUser, selectUser } from '../../../actions/user/userSlice.js';
import { setData, getData } from '../../util/localStorageHandler.js';



function ResetPassword() {

  let history = useHistory();

  let [code, setCode] = useState("");
  let [password, setPassword] = useState("");
  let [cnfPassword, setCnfPassword] = useState("");
  let [loading, setLoading] = useState(false);
  let [codeError, setCodeError] = useState("");
  let [passwordError, setPasswordError] = useState("");
  let [cnfPasswordError, setCnfPasswordError] = useState("");

  useEffect(() => {
    let data = getData();
    if (data) {
      // console.log('here 1');
      internalLogin(data.userType);
    } else {
      // console.log('here 2');
    }
  }, []);

  const internalLogin = (userType) => {
    console.log('here 3', userType);

    if (userType == SG_STUDENT) {
      history.push("/userdashboard");
    } else if (userType == SG_TEACHER) {
      history.push("/teachersdashboard");
    } else {
      Swal.fire("Error", 'userType not found', "error");
    }
  }

  const resetPass = async (e) => {
    e.preventDefault();
    // if(password && cnfpassword && password == cnfpassword)
    !code ? setCodeError("Give code") : setCodeError("");
    !password ? setPasswordError("Give a password") : setPasswordError("");
    !cnfPassword ? setCnfPasswordError("Give a password") : setCnfPasswordError("");
    password != cnfPassword ? setCnfPasswordError("Password mismatch") : setCnfPasswordError("");

    if (code && password && cnfPassword && password == cnfPassword) {

      setLoading(true);
      let dataToSend = {
        "code": code,
        "password": password
      }
      let url = `${API_BASEURL}/user/reset-password`;
      try {
        let response = await post(url, dataToSend);
        // console.log("response for reset-password === ", response);
        if (response.serverResponse.isError) {
          Swal.fire('Alert!', response.serverResponse.message, 'warning');
        } else {
          Swal.fire("success", response.serverResponse.message, "success");
          history.push("/signin");
        }

      } catch (error) {
        console.log(error);
        Swal.fire('Error!', error.message, 'error')
      }

    }

  }


  const clearError = () => {
    setPasswordError("");
    setCnfPasswordError("");
  }

  return (
    <>
      <Header />
      <section class="login sec_pad">
        <div class="container">
          <div class="heading">
            <h2 class="text-center">Reset Your Password</h2>
          </div>
          <div class="login-form">
            <form>
              <input type="text" class="form-control"
                placeholder="Enter code"
                value={code}
                onChange={e => setCode(e.target.value)}
              />
              {codeError && <span class="error-text">{codeError}</span>}

              <input type="text" class="form-control"
                placeholder="Enter new password"
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
              {passwordError && <span class="error-text">{passwordError}</span>}

              <input type="text" class="form-control"
                placeholder="Confirm password"
                value={cnfPassword}
                onChange={e => setCnfPassword(e.target.value)}
              />
              {cnfPasswordError && <span class="error-text">{cnfPasswordError}</span>}

              <button type="button" class="btn signin"
                onClick={e => resetPass(e)}
              >
                Reset
                </button>
            </form>
            <p class="text-center">Don't want to reset? <a href="/signin">Sign In</a></p>
          </div>
        </div>
      </section>
      <Footer />
    </>
  )

}

export default ResetPassword;