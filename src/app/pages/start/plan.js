import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setData, getData } from '../../util/localStorageHandler.js';



function Plan() {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [userId, setUserId] = useState('');
    const [plans, setPlans] = useState([]);

    useEffect(()=>{
        let data = getData();
        if (data) {
            setUserId(data.userId);
        }
        getPlans();
    },[])

    const getPlans= async()=> {

        setLoading(true);
        let path = `${API_BASEURL}/payment/get-subscription-plans`;
        let res = await post(path);
        console.log("Response from get plans", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode) {
            res.result && setPlans(res.result);
            setLoading(false);
        } else {
            setLoading(false);
            setPlans([]);
        }

    }

    const goToPlanPage= ()=> {

        if(userId && userId != ''){
            history.push({
                pathname: '/chooseplan'
            })
        }else{
            history.push({
                pathname: '/signin'
            })
        }

    }


    return (
        <>
            <section class="plans sec_pad bg-blue">
                <div class="container">
                    <div class="heading">
                        <h2 class="text-center">Choose Plan</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <div class="row">
                        {plans && plans.length>0 && plans.map((data,index)=>{
                            return !data.isUnlimited && <div class="col-lg-4" key={index}>
                                <div class="choose-plan text-center">
                                    <strong class="d-block">{data.planName && data.planName}</strong>
                                    <h3>{data.cost && '$' + data.cost}</h3>
                                    <span class="d-block">Per Month</span>
                                    <p>
                                        Plan Type: {!data.isUnlimited ? "Not Unlimited" : "Unlimited"}<br />
                                        No. of session: {data.noOfSession && data.noOfSession}
                                    </p>
                                </div>
                            </div>
                        })}
                        
                        {/* <div class="col-lg-3">
                            <div class="choose-plan text-center">
                                <strong class="d-block">Premium Membership</strong>
                                <h3>$150.00</h3>
                                <span class="d-block">Per Month</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div> */}
                        {!loading?
                            <a href="JavaScript:VOid(0);" class="nav-link btn btn-head" onClick={()=>goToPlanPage()}>Start Free Trial</a>
                        :null}
                    </div>
                </div>
            </section>
        </>
    )

}

export default Plan;