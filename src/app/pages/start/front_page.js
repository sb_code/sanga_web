import React, { Component } from 'react';

import FrontHeader from './header.js';
import FrontFooter from './footer.js';
import Banner from './banner.js';
import Classes from './classes.js';
import Works from './works.js';
import Poster from './poster.js';
import Plan from './plan.js';
import Faq from './faq.js';
import AppInfo from './app_info.js';



class FrontPage extends Component {
    render() {
        return (
            <>
                <FrontHeader />
                
                <Banner/>
                <Classes/>          
                <Works/>
                <Poster/>
                <Plan/>
                <Faq/>
                <AppInfo/>

                <FrontFooter />
            </>
        )
    }
}

export default FrontPage;