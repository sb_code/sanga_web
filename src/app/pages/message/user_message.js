import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";

import _ from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setData, getData } from '../../util/localStorageHandler.js';

import Header from '../layout/header.js';
import SubHeader from '../layout/subheader.js';
import Footer from '../layout/footer.js';


function UserMessage() {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [userId, setUserId] = useState(false);
    const [messages, setMessages] = useState([]);
    const [teachers, setTeachers] = useState([]);
    const [classes, setClasses] = useState([]);
    const [classId, setClassId] = useState("all");


    useEffect(() => {
        let data = getData();
        if (data) {
            setUserId(data.userId);
            getClasses(data.userId);
            getAllTeachers();
            getMessages(classId, data.userId);
        }
    }, []);

    const getClasses = async (userId) => {
        setLoading(true);
        let dataToSend = {
            "userId": userId
        };
        let path = `${API_BASEURL}/class/fetch-class-by-userid-of-student`;
        let res = await post(path, dataToSend);
        console.log("Response from fetch classes", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setLoading(false);
            res.result ? setClasses(res.result) : setClasses([]);
        } else {
            setClasses([]);
            setLoading(false)
        }
    }

    const getAllTeachers = async () => {
        setLoading(true);
        let dataToSend = {
            "page": '',
            "limit": ''
        }

        let path = `${API_BASEURL}/user/list-of-teacher`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            // console.log("Response from list teacher==", res.result);
            setLoading(false);
            res.result && setTeachers(res.result);
        } else {
            setLoading(false);
            setTeachers([]);
        }

    }

    const getMessages = async (classId, userId=null) => {
        setMessages([]);
        setLoading(true);
        let dataToSend= {};
        if (classId == "all") {
            dataToSend = {
                "userId": userId,
                "classId": "",
                "page": "",
                "limit": ""
            };
        } else {
            dataToSend = {
                "userId": "",
                "classId": classId,
                "page": "",
                "limit": ""
            };
        }
        let path = `${API_BASEURL}/message/get-message-by-class-id`;
        let res = await post(path, dataToSend);
        // console.log("Response from fetch message", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setLoading(false);
            res.result && setMessages(res.result);
        } else {
            setMessages([]);
            setLoading(false)
        }

    }

    const classSelection = (classId) => {
        // let selectedClass = classes[id];
        getMessages(classId, userId);
        setClassId(classId);
    }

    const nameOfTeacher = (userId) => {
        if (teachers) {
            let selectedTeacher = _.filter(teachers, { userId: userId });
            if (selectedTeacher && selectedTeacher[0].name) {
                return selectedTeacher[0].name;
            }
        } else {
            return null;
        }
    }

    return (
        <>
            <Header />
            <section class="msg-box">

                <SubHeader tag={'message'} />
                <div class="container">
                    <br /><br /><br />
                    <div class="login-form">
                        <div>
                            <label>Select class:</label>
                            <select
                                type="select"
                                class="form-control"
                                value={classId}
                                onChange={e => { setClassId(e.target.value); classSelection(e.target.value); }}
                            >
                                <option value="all">All Classes</option>
                                {classes && classes.map((data, index) => {
                                    return <option key={index} value={data.classId} selected={classId == data.classId ? true : false}>
                                        {data.className}
                                    </option>
                                })}
                            </select>
                        </div>
                    </div>
                    <div class="msg">
                        <div class="text-sec">
                            <ul>
                                {messages && messages.map((data, index) => {
                                    return data.messageType == "MESSAGE" ? <li key={index}>
                                        {data.userType == "admin" ?
                                            <div>Admin</div>
                                            : data.userType == "teacher" ?
                                                <div>{nameOfTeacher(data.userId) ? nameOfTeacher(data.userId) : null}</div>
                                                : <div></div>}
                                        <span>{data.message && data.message}</span>
                                        {/* &nbsp; &nbsp;<span onClick={() => deleteMessage(data.messageId)}><i class="fa fa-trash" aria-hidden="true"></i></span> */}
                                    </li>
                                        : data.messageType == "FILE" ? <li class="video-sec">
                                            {data.userType == "admin" ?
                                                <div>Admin</div>
                                                : data.userType == "teacher" ?
                                                    <div>{nameOfTeacher(data.userId) ? nameOfTeacher(data.userId) : null}</div>
                                                    : <div></div>}
                                            {/* <iframe width="100%" height="350" src={data.message} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> */}
                                            <video preload="auto" style={{ width: "100%" }}
                                                controls controlsList="nofullscreen nodownload"
                                            >
                                                <source src={data.message && data.message} />
                                            </video>
                                            {/* &nbsp; &nbsp;<span onClick={() => deleteMessage()}><i class="fa fa-trash" aria-hidden="true"></i></span> */}
                                        </li>
                                            : null
                                })}
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </>
    )

}

export default UserMessage;