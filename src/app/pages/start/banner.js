import React, { Component } from 'react';



class Banner extends Component {
    render() {
        return (
            <>
                <section class="banner">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="figcaption">
                                    <h1>
                                        Unleash their<br />
                                Creativity
                            </h1>
                                    <p>
                                        Live Classes for kids 4 - 10 year.<br />
                                Arts, Dance, Yoga, Singing, Drama, Spanish, Hindi and more...
                            </p>
                                    <a href="JavaScript:Void(0);" class="nav-link btn btn-head">Start Free Trail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="testimonial">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="testi-bg">
                                    <ul>
                                        <li class="left">
                                            <p><img src="images/testi.png" /></p>
                                            <strong>Jonathan Williamson</strong>
                                        </li>
                                        <li class="right">
                                            <p><img src="images/star.png" /> &nbsp; <img src="images/star.png" /> &nbsp; <img src="images/star.png" /> &nbsp; <img src="images/star.png" /> &nbsp; <img src="images/star.png" /></p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br />
                                        tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}

export default Banner;