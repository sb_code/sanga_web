import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setData, getData } from '../../util/localStorageHandler.js';
import Header from '../layout/header.js';
import Footer from '../layout/footer.js';


function TeacherProfile() {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [teacherId, setTeacherId] = useState('');
    const [about, setAbout] = useState('');

    useEffect(()=>{
        let selectedteacherId = history.location.state && history.location.state.id;
        setTeacherId(selectedteacherId);
        getTeacherDetails(selectedteacherId);
    },[])

    const getTeacherDetails= async(teacherId)=> {

        let dataToSend = {
            "userId": teacherId
        };
        setLoading(true);
        let path = `${API_BASEURL}/user/get-user-by-id`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setAbout(res.result && res.result);
            // console.log("teacher details === ", res.result);
            setLoading(false);
        }

    }

    
    return (
        <>
            <Header />

            <section class="msg-box">
                <div class="active-heading">
                    {/* <ul>
                        <li>Classes</li>
                        <li>Messages</li>
                        <li class="active">About Me</li>
                    </ul> */}
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="video-sec teacher-about">
                                {/* <iframe width="100%" height="350" src={about.videoURL && about.videoURL} frameborder="0" controls autoplay
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                                </iframe> */}
                                {about.videoURL &&<video width="100%" height="350" controls autoplay >
                                    <source src={about.videoURL} type="video/mp4" />
                                </video>}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="login-form teacher-about">
                                <form>
                                    <input type="text" readOnly= {true} class="form-control" value= {about.name && about.name} />
                                    <input type="text" readOnly= {true} class="form-control" value= {about.email && about.email} />
                                    <input type="text" readOnly= {true} class="form-control" value= {about.location && about.location} />
                                    <textarea type="text" readOnly= {true} class="form-control" value= {about.about && about.about} ></textarea>

                                    {/* <button type="button" class="btn signin">Save</button> */}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <Footer />
        </>
    )
    
}

export default TeacherProfile;