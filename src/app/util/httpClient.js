import axios from "axios";

export async function post(url, dataToSend = {}) {
  if (url) {
    try {
      let response = await axios.post(url, dataToSend);
      return response && response.data;
    } catch (error) {
      return null
    }
  } else {
    return null;
  }
}

/**
 * 
 * @param {File} file File to be uploaded
 * @param {String} uploadLocation Loctaion where file to be uploaded
 * @param {Object} dataToSend Other optional data
 * @param {function} progressCallBack Callback function for progerss of upload process. Provide object like this { total: number, tillNow: number, percentage: number }
 * @returns {Object} {isError: boolean, message: string, url: string}
 */
export async function postFile(file, uploadLocation, dataToSend = {}, progressCallBack = function () { }) {
  let url = 'https://us-central1-offerservicez-3d273.cloudfunctions.net/uploadImage';
  if (url && file && uploadLocation) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('bucketName', uploadLocation);
    dataToSend && Object.keys(dataToSend).map(key => {
      formData.append(key, dataToSend[key]);
    });
    try {
      let response = await axios.post(url, formData, {
        headers: { 'Content-Type': 'multipart/form-data' },
        onUploadProgress: (progressEvent) => {
          let tillNow = progressEvent.loaded;
          let total = progressEvent.total;
          let percentage = parseInt(Math.round((tillNow / total) * 100));
          progressCallBack({ total, tillNow, percentage });
        }
      });
      if (response && response.data && !response.data.isError && response.data.url) {
        return response.data.url
      } else {
        return null;
      }
    } catch (error) {
      return null
    }
  } else {
    return null;
  }
}
