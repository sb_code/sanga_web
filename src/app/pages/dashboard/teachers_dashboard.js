import React, { useState, useEffect } from 'react';
import Header from '../layout/header.js';
import SubHeader from '../layout/subheader.js';
import Footer from '../layout/footer.js';
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import withAuthorization from '../../Session/withAuthorization.js';
import { setData, getData } from '../../util/localStorageHandler.js';


function TeacherDashboard() {

	const history = useHistory();
	const [loading, setLoading] = useState(false);
	const [userId, setUserId] = useState('');
	const [myClass, setMyClass] = useState('');

	useEffect(() => {
		let data = getData();
		if (data) {
			// internalLogin(data.userType);
			setUserId(data.userId);
			let myClassList = getMyClassList(data.userId);

		} else {
			history.push({
				pathname: '/'
			})
		}
	}, []);


	const getMyClassList = async (userId) => {
		setLoading(true);
		let dataToSend = {
			"userId": userId,
			// "status": "",
			// "page": '',
			// "limit": ''
		}

		let path = `${API_BASEURL}/class/get-session-by-teacher-userId`;
		let res = await post(path, dataToSend);

		if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
			setMyClass(res.result && res.result)
			setLoading(false);
		} else {
			setMyClass("");
			setLoading(false);
		}

	}

	const classDuration = (startDate, endDate) => {

		let start = moment(startDate);
		let end = moment(endDate);

		let duration = moment.utc(moment(end, "DD/MM/YYYY HH:mm:ss").diff(moment(start, "DD/MM/YYYY HH:mm:ss"))).format("HH:mm");

		// console.log("Difference of time ==", duration);
		return duration;

	}

	const editClass = (classId, sessionId) => {

		console.log("classId == ", classId);
		console.log("classId == ", sessionId);

	}

	const startClass = async (classId, sessionId) => {

		setLoading(true);
		let dataToSend = {
			"userId": userId,
			"sessionId": sessionId
		}

		let path = `${API_BASEURL}/class/start-session-by-teacher`;
		let res = await post(path, dataToSend);

		if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
			Swal.fire("Success", "Successfully started", "Success");
			setLoading(false);
			getMyClassList(userId);
		} else {
			Swal.fire("Error", res.serverResponse.message, "Error");
			setLoading(false);
		}

	}

	const endClass = async (classId, sessionId) => {

		setLoading(true);
		let dataToSend = {
			userId: userId,
			sessionId: sessionId

		}

		let path = `${API_BASEURL}/class/end-session-by-teacher`;
		let res = await post(path, dataToSend);

		if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
			Swal.fire("Success", "Successfully ended", "Success");
			setLoading(false);
			getMyClassList(userId);
		} else {
			Swal.fire("Error", "Sorry! Try again later", "Error");
			setLoading(false);
		}

	}

	const cancelClass = async (classId, sessionId) => {

		setLoading(true);
		let dataToSend = {
			userId: userId,
			sessionId: sessionId
		}

		let path = `${API_BASEURL}/class/cancel-session-by-teacher`;
		let res = await post(path, dataToSend);

		if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
			Swal.fire("Success", "Successfully Canceled", "Success");
			setLoading(false);
			getMyClassList(userId);
		} else {
			Swal.fire("Error", res.serverResponse.message, "Error");
			setLoading(false);
		}

	}


	return (
		<>
			<Header />
			<section class="my-classes for-find-class">
				
				<SubHeader tag={'class'}/>
				<div class="container-fluid">
					<div class="heading">
						<h2>My classes</h2>
					</div>

					<div class="row">
						
					</div>
					<>
						{myClass && myClass.length > 0 && myClass.map((value, index) => {
							return <div class="container" key={index}>
								<div class="heading">

									<h4>{moment(value.date, "YYYY-MM-DD HH:mm:ss").format('dddd')}</h4>
								</div>

								<div class="row">
									{value.session && value.session.length > 0 && value.session.map((data, key) => {
										return <div class="col-lg-12" key={key}>
											<div class="outer-sec row">
												<div class="figure">
													<img src={data.bannerImage && data.bannerImage} alt="" />
												</div>

												<div class="figcaption">
													<h5>{data.name ? data.name : 'Class'}</h5>

													<span class="d-block">
														{data.ageGroupName ? data.ageGroupName : 'Age Group'} &nbsp;&nbsp; | &nbsp;&nbsp;
														{data.categoryName ? data.categoryName : 'Category'} &nbsp;&nbsp; | &nbsp;&nbsp;
														{value.date ? moment(value.date).format('DD-MM-YYYY') : 'Start Date'} &nbsp;&nbsp; | &nbsp;&nbsp;
														{data.startTime ? moment(data.startTime).format('hh.mm a') : 'Start Time'} &nbsp;&nbsp; | &nbsp;&nbsp;
														{data.startTime && data.endTime && classDuration(data.startTime, data.endTime)+' Hrs'} &nbsp;&nbsp; | &nbsp;&nbsp;

														{data.status && data.isActive && data.status == "upcoming" ?
																<span style={{ color: 'blue' }}>Upcomming</span>
																: data.status && data.isActive && data.status == "cancelled" ?
																	<span style={{ color: 'red' }}>Cancelled</span>
																	: data.status && data.isActive && data.status == "ongoing" ?
																		<span style={{ color: 'green' }}>Ongoing</span>
																		: <span>{data.status && data.status}</span>}

													</span>
													<p class="d-block">
														{/* <p onClick={() => goToTeacherProfile(data.teacherId && data.teacherId)} style={{ cursor: 'pointer' }}> */}
														{data.teacherName && data.teacherName}
														{/* </p> */}
													</p>
												</div>

												<div class="buttons">

													{data.status && data.isActive && data.status == "upcoming" ?
														<>
															<a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => startClass(data.classId, data.sessionId)}>Start Now</a>
															<a href="JavaScript:Void(0);" class="nav-link btn btn-head signup" onClick={() => cancelClass(data.classId, data.sessionId)}>Cancel</a>
														</>
															: data.status && data.isActive && data.status == "ongoing" ?
																<a href="JavaScript:Void(0);" class="nav-link btn btn-head signup" onClick={() => endClass(data.classId, data.sessionId)}>End</a>
																: null}

												</div>

											</div>
										</div>
									})}
								</div>
							</div>

						})}
					</>
				</div>
			</section>
			<Footer />
		</>
	)

}

const condition = authUser => authUser && authUser.userType === SG_TEACHER;
export default withAuthorization(condition)(TeacherDashboard);