import React, { Component } from 'react';



class Poster extends Component {
    render() {
        return (
            <>
                <section class="about">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="figure">
                                    <img src="images/abt1.jpg" />
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="figcaption">
                                    <h3>Lots to Learn</h3>
                                    <p>Your kids can wake up and workout, then learn geography, science, arts and so much more and end the day with a mindful yoga session.</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="figure">
                                    <img src="images/abt2.jpg" />
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="figcaption second">
                                    <h3>
                                        Kids love teacher-led classes
                            </h3>
                                    <p>Your kids can wake up and workout, then learn geography, science, arts and so much more and end the day with a mindful yoga session.</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="figure">
                                    <img src="images/abt3.jpg" />
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="figcaption third">
                                    <h3>
                                        Classes for the<br />
                                whole family
                            </h3>
                                    <p>Your kids can wake up and workout, then learn geography, science, arts and so much more and end the day with a mindful yoga session.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}

export default Poster;