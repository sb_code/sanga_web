import { createSlice } from '@reduxjs/toolkit';
import { setData } from '../../app/util/localStorageHandler';

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    "userId": "",
    "token": "",
    "name": "",
    "email": "",
    "phone": "",
    "countryCode": "",
    "username": "",
    "userType": "",
    "isInSubscription": false,

    "searchKey": "",
    "searching": false
  },
  reducers: {
    setUser: (state, action) => {
      state.userId = action.payload.userId;
      state.token = action.payload.token;
      state.name = action.payload.name;
      state.email = action.payload.email;
      state.phone = action.payload.phone;
      state.countryCode = action.payload.countryCode;
      state.username = action.payload.username;
      state.userType = action.payload.userType;
      state.isInSubscription = action.payload.isInSubscription;
    },

    setSearch: (state, action)=> {
      state.searchKey = action.payload.searchKey;
      state.searching = action.payload.searching;
    },

    logout: (state) => {
      console.log('HhhhhhhHHHHHhhh');
      state.userId = "";
      state.token = "";
      state.name = "";
      state.email = "";
      state.phone = "";
      state.countryCode = "";
      state.username = "";
      state.userType = "";
      state.isInSubscription = "";

      // Clear local storage
      setData("");
    },
  },
});

export const { setUser, setSearch, logout } = userSlice.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
// export const setUserAsync = amount => dispatch => {
//   setTimeout(() => {
//     dispatch(incrementByAmount(amount));
//   }, 1000);
// };

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.user.value)`
export const selectUser = state => state.user;

export default userSlice.reducer;

/**
 * https://redux-toolkit.js.org/api/createslice
 */