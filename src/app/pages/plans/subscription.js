import React, { useState, useEffect } from 'react';
import Header from '../layout/header.js';
import Footer from '../layout/footer.js';
import withAuthorization from '../../Session/withAuthorization.js';
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setData, getData } from '../../util/localStorageHandler.js';


function Subscription() {


    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [userId, setUserId] = useState('');
    const [plans, setPlans] = useState([]);
    const [selectPlan, setSelectPlan] = useState('');
    const [inPlan, setInPlan] = useState(false);
    const [selectedPlan, setSelectedPlan] = useState('');


    useEffect(() => {
        let data = getData();
        if (data) {
            setUserId(data.userId);
            getPlans();
            getUserDetails(data.userId);
        } else {
            history.push({
                pathname: '/'
            })
        }
    }, [])

    const getPlans = async () => {

        setLoading(true);
        let path = `${API_BASEURL}/payment/get-subscription-plans`;
        let res = await post(path);
        // console.log("Response from get plans", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode) {
            res.result && setPlans(res.result);
            setLoading(false);
        } else {
            setLoading(false);
            setPlans([]);
        }

    }

    const getUserDetails = async (userId) => {

        setLoading(true);
        let dataToSend = {
            'userId': userId
        };
        let path = `${API_BASEURL}/user/get-student-by-user-id`;
        let res = await post(path, dataToSend);
        // console.log("Response from get user details==", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode) {
            res.result && res.result.isInSubscription ? setInPlan(true) : setInPlan(false);
            res.result && res.result.isInSubscription ? setSelectedPlan('') : setSelectedPlan('');
            setLoading(false);
        } else {
            setLoading(false);
            // setPlans([]);
        }

    }

    const choosePlan = (index) => {

        let selectedPlan = plans[index];
        setSelectPlan(selectedPlan);

    }

    const changePlan = () => {



    }

    const payment = (status) => {
        if (selectPlan && selectPlan != '') {

            history.push({
                pathname: '/payment',
                state: {
                    planId: selectPlan.subscriptionPlanId,
                    planName: selectPlan.planName,
                    amount: selectPlan.cost,
                    status: status
                }
            })

        } else {
            Swal.fire("Error", "Please choose a plan", "Error");
        }

    }


    return (

        <div class="row">

            {plans && plans.length > 0 && plans.map((data, index) => {
                return <div class="col-lg-3" key={index}>
                    <div class="choose-plan text-center">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input"
                                checked={selectedPlan && selectedPlan.subscriptionPlanId && data.subscriptionPlanId == selectedPlan.subscriptionPlanId ? true : false}
                                name="optradio" onClick={() => choosePlan(index)}
                            />
                        </label>

                        <strong class="d-block">{data.planName && data.planName}</strong>
                        <h3>{data.cost && '$' + data.cost}</h3>
                        <span class="d-block">Per Month</span>
                        <p>
                            Plan Type: {!data.isUnlimited ? "Not Unlimited" : "Unlimited"}<br />
                                                No. of session: {data.noOfSession && data.noOfSession}
                        </p>
                    </div>
                </div>
            })}

            {!loading ?
                <>
                    {!inPlan ?
                        <a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => payment('Subscription')}>Start Free Trial</a>
                        : <a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => changePlan()}>Change Plan</a>
                    }

                    {/* <a href="JavaScript:Void(0);" class="nav-link btn btn-head" onClick={() => payment('Payment')}>DIrect Payment</a> */}
                </>
                : null}
        </div>


    );


}

export default Subscription;
