import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';
import Swal from 'sweetalert2';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setData, getData } from '../../util/localStorageHandler.js';

function Classes() {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [userId, setUserId] = useState('');
    const [userType, setUserType] = useState('');
    const [categoryList, setCategoryList] = useState([]);
    const [sessionList, setSessionList] = useState([]);
    const [filter, setFilter] = useState({ 'category': '', 'ageGroup': '', 'fromDate': '', 'toDate': '', 'time': '' });


    useEffect(() => {
        let data = getData();
        getCategory();
        getLatestSession();
        if (data) {
            // internalLogin(data.userType);
            setUserId(data.userId);
            setUserType(data.userType);
        } else {
            setUserId('');
            setUserType('');
        }
    }, []);


    const getCategory = async () => {

        setLoading(true);

        let dataToSend = {};
        let path = `${API_BASEURL}/class/find-category`;

        let res = await post(path, dataToSend);
        // console.log("Response from get category ===", res);
        if (res && res.serverResponse && !res.serverResponse.isError && res.serverResponse.statuscode == 200) {
            setCategoryList(res.result && res.result)
            setLoading(false);
        } else {
            setCategoryList("");
            setLoading(false);
        }

    }

    const getLatestSession = async () => {
        setLoading(true);
        let dataToSend = {
            "page": 1,
            "limit": 3
        }
        let path = `${API_BASEURL}/class/get-session-with-pagination`;
        let res = await post(path, dataToSend);

        if (res && res.serverResponse && !res.serverResponse.isError) {
            if (res.serverResponse.statuscode != 200) {
                Swal.fire("Error", res.serverResponse.message, "Error");
            } else {
                setSessionList(res.result);
                setLoading(false);
            }
            // console.log("Response from APi -- sessions //", sessionList);
        } else {
            setLoading(false);
        }

    }

    const filterByCategory = (categoryId) => {
        if (!userId || userId == '' || userType == SG_STUDENT) {
            filter.category = categoryId;
            setFilter(filter);
            history.push({
                pathname: '/findclass',
                state: {
                    'filter': filter
                }
            })
        } else if (userId && userType == SG_TEACHER) {

        }
    }

    // For showing any session details - 
    const viewSessionDetails = (classId, sessionId) => {

        // history.push('/classdetails/'+sessionId);
        history.push({
            pathname: '/classdetails/' + `${sessionId}`,
            state: {
                'session': sessionId,
                'class': classId
            }
        })

    }

    // For enrolling the session -
    const enrollSession = async (classId, sessionId) => {

        if (userId) {
            setLoading(true);
            let dataToSend = {
                userId: userId,
                classId: classId,
                sessionId: sessionId
            }

            let path = `${API_BASEURL}/class/signup-session-by-student`;
            let res = await post(path, dataToSend);

            if (res && res.serverResponse && !res.serverResponse.isError) {
                setLoading(false);
                Swal.fire("Success", "Successfully enrolled", "Success");
                getLatestSession();

            } else {
                setLoading(false);
                Swal.fire("Error", res.serverResponse.message, "Error");
            }
        } else {
            Swal.fire("Error", "Sorry! You are not logged in", "Error");
        }


    }

    // For those who are not logged in yet - 
    const signUp = () => {

        // history.push({
        //     pathname: '/signin',
        //     state: {}
        // })

        window.location.replace('/signin');

    }

    // FOr checking whether the user is enrolled to it or not -
    const isEnrolled = (enrolledStudent) => {
        if (userId) {
            if (enrolledStudent && enrolledStudent.length > 0) {
                for (let i = 0; i < enrolledStudent.length; i++) {
                    if (enrolledStudent[i].userId == userId) {
                        return true
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    // For enrolled user to join - 
    const joinSession = (classId, sessionId) => {

        history.push({
            pathname: '/joinclass/' + `${sessionId}`,
            state: {
                'session': sessionId,
                'class': classId
            }
        })

    }


    return (
        <>
            <section class="rcm-class">
                <div class="container">
                    <div class="heading text-center">
                        <h2>Live Online Classes</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <div class="active-heading find-class-one for-home">
                    <div class="container">
                        <ul>
                            <li class="active" style={{ cursor: "pointer" }}>All</li>
                            {categoryList && categoryList.map((data, index) => {
                                return <li key={index} onClick={() => filterByCategory(data.categoryId)} style={{ cursor: "pointer" }}>
                                    {data.categoryName}
                                </li>
                            })}
                            {/* <li>Dance</li>
                            <li>Yoga</li>
                            <li>Music</li> */}
                        </ul>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        {sessionList && sessionList.map((data, index) => {
                            return <div class="col-lg-4" key={index}>
                                <div class="price_item">
                                    <div class="figure">
                                        {/* <img src="images/img1.png" alt="" /> */}
                                        <img src={data.bannerImage && data.bannerImage} alt="" />
                                    </div>
                                    <div class="figcaption">
                                        <h5 class="text-uppercase">{data.name && data.name}</h5>
                                        <strong>{data.startTime && moment(data.startTime).format('hh.mm a')}</strong>
                                        <span class="d-block">{moment(data.startDate).format('ll')}</span>
                                        <p class="d-block">{data.teacherName && data.teacherName}</p>

                                        <a href="JavaScript:Void(0);" onClick={() => viewSessionDetails(data.classId, data.sessionId)}>Details</a>
                                        {userId && !isEnrolled(data.enrolledStudent) ?
                                            <a href="JavaScript:Void(0);" class="signup" onClick={() => enrollSession(data.classId, data.sessionId)}>Enroll</a>
                                            : null}
                                        {userId && isEnrolled(data.enrolledStudent) ?
                                            <a href="JavaScript:Void(0);" class="signup" onClick={() => joinSession(data.classId, data.sessionId)}>Join</a>
                                            : null}
                                        {!userId ?
                                            <a href="JavaScript:Void(0);" class="signup" onClick={() => signUp()}>Sign me up</a>
                                            : null}
                                    </div>
                                </div>
                            </div>
                        })}
                        {/* <div class="col-lg-4">
                            <div class="price_item">
                                <div class="figure">
                                    <img src="images/img1.png" alt="" />
                                </div>
                                <div class="figcaption">
                                    <h5 class="text-uppercase">Lorem ipsum dolor sit amet.</h5>
                                    <strong>10:30am</strong>
                                    <span class="d-block">20th August, 2020</span>
                                    <p class="d-block">Jhon Williams</p>
                                    <a href="#">Details</a>
                                    <a href="#" class="signup">Sign me up</a>
                                </div>
                            </div>
                        </div>
                         */}
                    </div>
                </div>
            </section>
        </>
    )

}

export default Classes;