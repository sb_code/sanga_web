import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom'
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import store from './app/store';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';

import SignUp from './app/pages/auth/signup';
import SignIn from './app/pages/auth/signin';
import ForgotPassword from './app/pages/auth/forgot_password';
import ResetPassword from './app/pages/auth/reset-password';


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      {/* <App /> */}
      <BrowserRouter>
        <Switch>
          {/* <Route exact path='/' name='Front Page' component={FrontPage} /> */}
          <Route path='/signup' name='Sign Up' component={SignUp} />
          <Route path='/signin' name='Sign In' component={SignIn} />
          <Route path='/forgot-password' name='Forgot Password' component={ForgotPassword} />
          <Route path='/reset-password' name='Reset Password' component={ResetPassword} />
          <Route component={App} />
        </Switch>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
