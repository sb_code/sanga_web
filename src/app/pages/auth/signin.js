import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import Swal from 'sweetalert2'
import Header from '../layout/header.js';
import Footer from '../layout/footer.js';
import { GoogleLogin } from 'react-google-login';
import { post } from '../../util/httpClient.js';
import { API_BASEURL, SG_STUDENT, SG_TEACHER } from '../../constants/global.js';
import { setUser, selectUser } from '../../../actions/user/userSlice.js';
import { setData, getData } from '../../util/localStorageHandler.js';

function SignIn() {
  const user = useSelector(selectUser);
  const dispatch = useDispatch();
  let history = useHistory();

  let [username, setUsername] = useState("");
  let [password, setPassword] = useState("");
  let [email, setEmail] = useState("");

  let [loading, setLoading] = useState(false);
  let [usernameError, setUsernameError] = useState("");
  let [passwordError, setPasswordError] = useState("");
  // let [emailError, setEmailError] = useState("");

  useEffect(() => {
    let data = getData();
    if (data) {
      // console.log('here 1');
      internalLogin(data.userType);
    }else{
      // console.log('here 2');
    }
  }, []);

  const goToForgotPassword = () => {
    history.push("/forgot-password");
  }

  const responseGoogle = async (response) => {
    if (!response || response.error) {
      Swal.fire('Error!', response.error, 'error')
    } else {
      let socialId = response.googleId;
      let name = response.profileObj && response.profileObj.name ? response.profileObj.name : '';
      let username = name.toLowerCase().replace(/[^A-Z0-9]+/ig, "_");
      let email = response.profileObj && response.profileObj.email ? response.profileObj.email : '';
      let socialType = 'google';
      socialSignIn(socialId, name, username, email, socialType);
    }
  }

  const getUserBySocialId = async (socialId) => {
    let url = `${API_BASEURL}/user/get-user-by-social-id`;
    let dataToSend = { socialId };
    let requestType = null;
    try {
      let response = await post(url, dataToSend);
      // console.log(response);
      if (!response.result) {
        requestType = "signup";
      } else {
        requestType = "signin";
      }
    } catch (error) {
      console.log(error);
      Swal.fire('Error!', error.message, 'error')
    }
    return requestType;
  }

  const socialSignIn = async (socialId, name, username, email, socialType) => {
    let requestType = await getUserBySocialId(socialId);
    let url = `${API_BASEURL}/user/social-sign-in`;
    let dataToSend = {
      name,
      username,
      email,
      socialId,
      socialType,
      requestType
    }
    // console.log('social-sign-in  dataToSend', dataToSend);
    if (requestType == "signup" && !email) {
      Swal.fire({
        title: 'Enter your email',
        input: 'text',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: false,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true,
        preConfirm: (email) => {
          if (email) {
            return email
          } else {
            Swal.showValidationMessage(
              `Request failed: Email`
            )
          }
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then(async (result) => {
        let email = result.value;
        dataToSend.email = email;
        try {
          let response = await post(url, dataToSend);
          // console.log('social-sign-in  response', response);
          if (response.serverResponse.isError) {
            Swal.fire('Alert!', response.serverResponse.message, 'warning')
          } else {
            Swal.fire("Success", response.serverResponse.message, "success");
            // store in redux
            dispatch(setUser(response.result));
            // save local storage
            setData(response.result);
            // Redirect to student dashbosrd.
            history.push("/userdashboard");
          }
        } catch (error) {
          console.log(error);
          Swal.fire('Error!', error.message, 'error')
        }
      })
    } else {
      try {
        let response = await post(url, dataToSend);
        // console.log('social-sign-in  response', response);
        if (response.serverResponse.isError) {
          Swal.fire('Alert!', response.serverResponse.message, 'warning')
        } else {
          Swal.fire("Success", response.serverResponse.message, "success");
          // store in redux
          dispatch(setUser(response.result));
          // save local storage
          setData(response.result);
          // Redirect to student dashbosrd.
          history.push("/userdashboard");
        }
      } catch (error) {
        console.log(error);
        Swal.fire('Error!', error.message, 'error')
      }
    }
  }

  const onKeyUpHandler= (event)=> {

    let code = event.keyCode;
    if (code === 13) {
      emailLogin();
    }

  }

  const emailLogin = async () => {
    if (username && password) {
      setLoading(true);
      // console.log(username, password);
      let url = `${API_BASEURL}/user/sign_in`;
      let dataToSend = {
        username,
        password
      }
      try {
        let response = await post(url, dataToSend);
        // console.log(response);
        if (response.serverResponse.isError) {
          Swal.fire('Alert!', response.serverResponse.message, 'warning')
        } else {
          Swal.fire("success", 'Log in success', "success");
          // store in redux
          dispatch(setUser(response.result));
          // save local storage
          setData(response.result);
          // Redirect to student dashbosrd or teacher dashboard depend on userType.
          if (response.result.userType == SG_STUDENT) {
            history.push("/userdashboard");
          } else if (response.result.userType == SG_TEACHER) {
            history.push("/teachersdashboard");
          } else {
            Swal.fire("Error", 'userType not found', "error");
          }
        }
      } catch (error) {
        console.log(error);
        Swal.fire('Error!', error.message, 'error')
      }
      setLoading(false);
    } else {
      if (!username) setUsernameError("Enter your username")
      if (!password) setPasswordError("Enter valide password.")
    }
  }

  const internalLogin = (userType) => {
    console.log('here 3',userType);

    if (userType == SG_STUDENT) {
      history.push("/userdashboard");
    } else if (userType == SG_TEACHER) {
      history.push("/teachersdashboard");
    } else {
      Swal.fire("Error", 'userType not found', "error");
    }
  }

  return (
    <>
      <Header />
      <section class="login sec_pad">
        <div class="container">
          <div class="heading">
            <h2 class="text-center">Log In</h2>
          </div>
          <div class="login-form">
            <div>
              <input
                type="text"
                class="form-control"
                placeholder="Enter your username"
                value={username}
                onChange={e => {setUsername(e.target.value); setUsernameError('');}}
              />
              {usernameError && <h8 class="error-text">{usernameError}</h8>}

              <input
                type="password"
                class="form-control"
                placeholder="Enter your password"
                value={password}
                onChange={e => {setPassword(e.target.value); setPasswordError('');}}
                onKeyUp={event => onKeyUpHandler(event)}
              />
              {passwordError && <h8 class="error-text">{passwordError}</h8>}

              <span class="d-block" style= {{cursor: "pointer"}}
                onClick={goToForgotPassword}>
                Forgot Password?
              </span>
              {
                loading ?
                  <button type="button" class="btn signin" disabled={loading}>Sign In</button>
                  :
                  <button type="button" class="btn signin" onClick={emailLogin}>Sign In</button>
              }

              <strong class="text-center d-block">OR</strong>

              {
                loading ?
                  <button type="button" class="btn signin google" disabled={loading}>Sign In with Google</button>
                  :
                  <GoogleLogin
                    clientId="1022389657209-n4dast0j80gnn4mfqamttv8gr5rbonqn.apps.googleusercontent.com"
                    render={renderProps => (
                      <button type="button" class="btn signin google" onClick={renderProps.onClick} disabled={renderProps.disabled}>Sign In with Google</button>
                    )}
                    buttonText="Login"
                    onSuccess={responseGoogle}
                    onFailure={responseGoogle}
                    cookiePolicy={'single_host_origin'}
                  />
              }


            </div>
            <p class="text-center">Don't have an account? <a href="/signup">Sign Up</a></p>
          </div>
        </div>
      </section>

      <Footer />
    </>
  )
}

export default SignIn;