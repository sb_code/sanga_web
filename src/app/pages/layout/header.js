import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import Swal from 'sweetalert2';
import { logout, setSearch, selectUser } from '../../../actions/user/userSlice';
import { SG_STUDENT, SG_TEACHER } from '../../constants/global';
import { setData, getData } from '../../util/localStorageHandler.js';


function Header(props) {
	
	const user = useSelector(selectUser);
	const dispatch = useDispatch();
	let history = useHistory();

	const [searchKey, setSearchKey] = useState('');
	const [searching, setSearching] = useState(false);
	const [userId, setUserId] = useState('');
	const [userType, setUserType] = useState('');

	useEffect(() => {
		let data = getData();
		if (data) {
			setUserId(data.userId);
			setUserType(data.userType);
		}
		setSearchKey(user.searchKey);
		setSearching(user.searching);
		// console.log("consoling user from component did mount of layout Header", user);
	}, [])

	useEffect(() => {
		setSearchKey(user.searchKey);
		setSearching(user.searching);
	}, [setSearchKey, setSearching, user])

	const searchBy = async (event) => {
		event.preventDefault();

		if (!searchKey || searchKey == '') {
			Swal.fire("Error", "Please search by class or category name", "Error");
		} else {
			let payload = {
				"searchKey": searchKey,
				"searching": true
			};
			dispatch(setSearch(payload));

			if (history.pathname && history.pathname == '/findclass') {
				console.log("I'm here");
			} else {
				history.push({
					pathname: '/findclass',
					state: {}
				});
			}

		}

	}

	const cancelSearch = (e) => {
		e.preventDefault();
		let payload = {
			"searchKey": '',
			"searching": false
		};
		dispatch(setSearch(payload));
		
	}

	const logMeOut = () => {
		dispatch(logout());
		window.location.reload();
	}

	// Redirect to home pge/dashboard if ogged in -- 
	const goToHomepage= ()=> {

		if(userId && userId != ''){
			if(userType == SG_STUDENT){
				history.push({
					pathname: '/userdashboard',
					state: {}
				})
			}else if(userType == SG_TEACHER){
				history.push({
					pathname: '/teachersdashboard',
					state: {}
				})
			}
		}else{
			history.push({
				pathname: '/',
				state: {}
			})
		}

	}

	return (
		<>
			<header class="header_area">
				<nav class="navbar navbar-expand-lg">
					<a class="navbar-brand" href="JavaScript:Void(0);" onClick= {()=>goToHomepage()}><img src="../images/logo.png" /></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onClick={e => e.preventDefault()}>
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						{user && user.userId && user.userType === SG_STUDENT ?
							<form class="form-inline ml-auto">
								<input class="form-control" type="search" value={searchKey} placeholder="What are you loking for?" aria-label="Search" onChange={(e) => setSearchKey(e.target.value)} />
								{!searching ?
									<button class="btn search" type="submit" onClick={(e) => searchBy(e)}><i class="fa fa-search"></i></button>
									: <button class="btn search" type="submit" onClick={(e) => cancelSearch(e)}><i class="fa fa-times"></i></button>
								}
							</form>
							: null}
						{!user || !user.userId ?
							<form class="form-inline ml-auto">
								<input class="form-control" type="search" value={searchKey} placeholder="What are you loking for?" aria-label="Search" onChange={(e) => setSearchKey(e.target.value)} />
								{!searching ?
									<button class="btn search" type="submit" onClick={(e) => searchBy(e)}><i class="fa fa-search"></i></button>
									: <button class="btn search" type="submit" onClick={(e) => cancelSearch(e)}><i class="fa fa-times"></i></button>
								}
							</form>
							: null}
						<ul class="navbar-nav">
							{/* <li class="nav-item">
								<a class="nav-link" href="/findclass">Find Classes</a>
							</li> */}
							{
								user && user.userId ?
									user.userType === SG_STUDENT ?
										<>
											<li class="nav-item">
												<a class="nav-link" href="/findclass">Find Classes</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" href="/userdashboard">Dashboard</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" href="/chooseplan">Payment</a>
											</li>
										</>
										:
										<li class="nav-item">
											<a class="nav-link" href="/teachersdashboard">Dashboard</a>
										</li>
									:
									<>
										<li class="nav-item">
											<a class="nav-link" href="/findclass">Find Classes</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/jointeacher">Join as a Teacher</a>
										</li>
									</>
							}
							{
								user && user.userId ?
									<li class="nav-item">
										<a class="nav-link btn btn-head" href="javascript:void(0);" onClick={logMeOut}>Log Out</a>
									</li>
									:
									<li class="nav-item">
										<a class="nav-link btn btn-head" href="/signin">Log In</a>
									</li>
							}

						</ul>
					</div>
				</nav>
			</header>
		</>
	)
}

export default Header;